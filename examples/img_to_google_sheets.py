#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from gspread_formatting import Color
from PIL import Image

from google_sheet_writer import Cf, FCell, GoogleSheetWriter


LIMIT = 300 * 300


class ImageWriter(GoogleSheetWriter):
    def __init__(self, *args, **kwargs):
        self.image_path = kwargs.pop("image_path")
        self.resize = kwargs.pop("resize")
        self.im = Image.open(self.image_path)
        if self.resize:
            w, h = self.resize.split("x")
            self.im = self.im.resize((int(w), int(h)), resample=Image.LANCZOS)
        width, height = self.im.size
        if width * height > LIMIT:
            raise Exception(
                f"images more than {LIMIT} pixels are not supported, "
                "try --resize option"
            )
        super().__init__(*args, **kwargs)

    def write(self):
        sheet1 = self.get_worksheet("Sheet1", remove=True)
        sheet1.hide_gridlines()

        width, height = self.im.size
        for row_index in range(height):
            row = []
            for col in range(width):
                r, g, b = self.im.getpixel((col, row_index))
                row.append(
                    FCell(
                        None,
                        Cf.background_color(Color(r / 255, g / 255, b / 255)),
                        width=3,
                        height=4,
                    )
                )
            sheet1.add_block(row, horizontal=True, add_newline=True)
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--image", "-i", help="path to image file", required=True)
    parser.add_argument("--spreadsheet", "-s", help="google sheet id", required=True)
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    parser.add_argument("--resize", "-r", help="e.g. --resize 200x200")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)
    iw = ImageWriter(gc, args.spreadsheet, image_path=args.image, resize=args.resize)
    iw.write()


if __name__ == "__main__":
    main()
