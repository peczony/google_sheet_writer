#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from google_sheet_writer import (
    Cf,
    FCell,
    GoogleSheetWriter,
    color_from_hex
)
from studchr_tables_common import (
    init_worksheet,
)


class SimpleExample(GoogleSheetWriter):
    def make(self):
        self.wks = self.get_worksheet("Test", remove=True)
        init_worksheet(self.wks)
        self.wks.cursor.hblock_n([FCell(
            "Hello world", Cf.BOLD + Cf.font_size(24) + Cf.font_family("Vollkorn") + Cf.background_color(color_from_hex("ff0000")) + Cf.text_color(color_from_hex("00ff00")), height=40
        )])
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = SimpleExample(gc, args.spreadsheet_id)
    writer.make()


if __name__ == "__main__":
    main()
