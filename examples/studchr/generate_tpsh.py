#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    FCell,
)
from studchr_tables_common.si import SiMaker, get_place
from studchr_tables_common.utils import (
    LIGHT_GRAY_BG,
    LIGHT_GREEN_BG,
    init_worksheet,
)

N_PARTICIPANTS = 200
OTBOR_HEADER = [
    FCell("Игрок", LIGHT_GRAY_BG, width=180),
    FCell("Σ", LIGHT_GRAY_BG + Cf.RIGHT, width=40),
    FCell("Σ+", LIGHT_GRAY_BG + Cf.RIGHT, width=40),
    FCell(50, LIGHT_GRAY_BG + Cf.RIGHT, width=25),
    FCell(40, LIGHT_GRAY_BG + Cf.RIGHT, width=25),
    FCell(30, LIGHT_GRAY_BG + Cf.RIGHT, width=25),
    FCell(20, LIGHT_GRAY_BG + Cf.RIGHT, width=25),
    FCell(10, LIGHT_GRAY_BG + Cf.RIGHT, width=25),
]
INIT_PO = [
    [1, 8, 9, 16],
    [2, 7, 10, 15],
    [3, 6, 11, 14],
    [4, 5, 12, 13],
]


class TpshMaker(SiMaker):
    def add_reg(self):
        self.reg = self.get_worksheet("Регистрация", remove=True)
        init_worksheet(self.reg)
        cur = self.reg.cursor
        cur.freeze_row()
        cur.hblock_n(
            [
                FCell("Игрок", Cf.BOLD, width=180),
                FCell("Оплата", Cf.BOLD + Cf.CENTER, width=70),
            ]
        )
        self.players_start = cur.clone()
        cur.shift(x=1)
        cur.vblock([FCell(Checkbox(), Cf.CENTER)] * N_PARTICIPANTS)

    def make_si(self):
        self.add_reg()
        self.otbor = self.get_worksheet("Письменный отбор", remove=True)
        self.dummy = self.get_worksheet("_dummy", remove=True, hidden=True)
        init_worksheet(self.dummy)
        init_worksheet(self.otbor)
        self.otbor.cursor.freeze_row()
        self.otbor_players_start = self.otbor.spawn_cursor(x=1, y=2)
        pism_otbor_match = self.add_si_match(
            self.dummy.cursor,
            self.otbor.cursor,
            [
                f"={self.players_start.clone_shift(y=n):f}"
                for n in range(N_PARTICIPANTS)
            ],
            themes=10,
            match_id="PO",
        )
        crutch = self.otbor.spawn_cursor(x=1, y=1)
        crutch.hblock(["Письменный отбор"])
        crutch.shift(x=1).hblock([FCell("М", hidden=True)])
        self.otbor_hidden = self.get_worksheet(
            "_otbor_hidden", remove=True, hidden=True
        )
        init_worksheet(self.otbor_hidden)
        self.otbor_hidden.cursor.hblock_n(OTBOR_HEADER)
        self.otbor_sort_start = self.otbor_hidden.cursor.clone()
        self.otbor_hidden.cursor.vblock(
            [
                f"={self.otbor_players_start.clone_shift(y=n):f}"
                for n in range(N_PARTICIPANTS)
            ]
        )
        self.otbor_hidden.cursor.vblock(
            [
                f"={pism_otbor_match.sums_cursor.clone_shift(y=n + 1):f}"
                for n in range(N_PARTICIPANTS)
            ]
        )
        self.otbor_hidden.cursor.vblock(
            [
                f"={pism_otbor_match.sum_plus_cursor.clone_shift(y=n + 1):f}"
                for n in range(N_PARTICIPANTS)
            ]
        )
        for c in range(1, 6):
            self.otbor_hidden.cursor.vblock(
                [
                    f"={pism_otbor_match.sum_plus_cursor.clone_shift(x=c, y=n + 1):f}"
                    for n in range(N_PARTICIPANTS)
                ]
            )
        self.otbor_itogi = self.get_worksheet("Итоги отбора", remove=True)
        init_worksheet(self.otbor_itogi)
        self.otbor_itogi.hblock([FCell("#", Cf.RIGHT + LIGHT_GRAY_BG, width=40)])
        self.otbor_itogi.cursor.hblock_n(OTBOR_HEADER)
        number_cursor = self.otbor_itogi.cursor.clone_shift(x=-1)
        self.otbor_itogi_start = self.otbor_itogi.cursor.clone()
        cur_green = self.otbor_itogi_start.clone_shift(x=-1)
        for _ in range(16):
            cur_green.hblock_n([FCell(None, LIGHT_GREEN_BG)] * 9)
        number_cursor.vblock(list(range(1, N_PARTICIPANTS + 1)))
        self.otbor_itogi.cursor.hblock_n(
            [
                f"=SORT({self.otbor_sort_start:f}:{self.otbor_sort_start.clone_shift(y=N_PARTICIPANTS - 1, x=7)}, 2, FALSE, 3, FALSE, 4, FALSE, 5, FALSE, 6, FALSE)"
            ]
        )
        self.otbor_itogi.cursor.vblock([None] * (N_PARTICIPANTS - 1))
        self.po = self.get_worksheet("Плей-офф", remove=True)
        self.po_prot = self.get_worksheet("Плей-офф (протоколы)", remove=True)
        init_worksheet(self.po)
        init_worksheet(self.po_prot)
        otbor_finished_checkbox = self.po.cursor.clone()
        self.po.cursor.hblock_n(
            [FCell(Checkbox(), Cf.RIGHT), "Письменный отбор закончен"]
        )
        self.po_prot.cursor.hblock_n(
            [FCell("Четвертьфиналы", Cf.font_size(13), height=30)]
        )
        self.po.cursor.shift(y=1)
        po_start = self.po.cursor.clone()
        po_1 = []
        for match_comp in INIT_PO:
            po_1.append(
                self.add_si_match(
                    table_cursor=self.po.cursor,
                    protocol_cursor=self.po_prot.cursor,
                    participants=[
                        f"""=IF({otbor_finished_checkbox:f}, {self.otbor_itogi_start.clone_shift(y=n - 1):f}, "Отбор-{n}")"""
                        for n in match_comp
                    ],
                    themes=9,
                    table_bg_color=LIGHT_GRAY_BG,
                    add_newline=True,
                    add_tiebreak=True,
                )
            )
        self.po.cursor.set(x=self.po.max_col() + 1, y=po_start.y)
        self.po.cursor.hblock([FCell(None, width=40)])
        self.po_prot.cursor.hblock_n([FCell("Полуфиналы", Cf.font_size(13), height=30)])
        for participants in (
            [
                get_place(po_1[0], 1),
                get_place(po_1[1], 2),
                get_place(po_1[2], 2),
                get_place(po_1[3], 1),
            ],
            [
                get_place(po_1[0], 2),
                get_place(po_1[1], 1),
                get_place(po_1[2], 1),
                get_place(po_1[3], 2),
            ],
        ):
            self.add_si_match(
                table_cursor=self.po.cursor,
                protocol_cursor=self.po_prot.cursor,
                participants=participants,
                themes=9,
                table_bg_color=LIGHT_GRAY_BG,
                add_newline=True,
                add_tiebreak=True,
            )
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = TpshMaker(gc, args.spreadsheet_id)
    writer.make_si()


if __name__ == "__main__":
    main()
