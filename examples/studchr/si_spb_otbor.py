#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import random
from typing import NamedTuple
import enum

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    Cursor,
    FCell,
    col_to_a,
)
from studchr_tables_common import (
    GREEN_BG,
    LIGHT_GRAY_BG,
    RED_BG,
    init_worksheet,
    SiMaker,
)


class PlayoffEnum(enum.Enum):
    PO24_2 = "po24_2"
    PO16 = "po16"


def enumtype(astring):
    try:
        return PlayoffEnum[astring.upper()]
    except KeyError:
        msg = ", ".join([t.name.lower() for t in PlayoffEnum])
        msg = f"PlayoffEnum: use one of {msg}"
        raise argparse.ArgumentTypeError(msg)


QUAL_ROUND_2 = [
    [1, 24, 33],
    [2, 23, 31],
    [3, 22, 36],
    [4, 21, 25],
    [5, 20, 34],
    [6, 19, 29],
    [7, 18, 30],
    [8, 17, 35],
    [9, 16, 32],
    [10, 15, 26],
    [11, 14, 28],
    [12, 13, 27],
]
PLAYOFF_COMPOSITION = {
    PlayoffEnum.PO24_2: dict(
        PO_2=[
            [1, 6, 7, 12],
            [2, 5, 8, 11],
            [3, 4, 9, 10],
            [13, 18, 19, 24],
            [14, 17, 20, 23],
            [15, 16, 21, 22],
        ],
        PO_3=[[1, 4, 5], [2, 3, 6], [7, 12, 13, 18], [8, 11, 14, 17], [9, 10, 15, 16]],
        PO_4=[[1, 2, 3, 4], [5, 8, 9, 12], [6, 7, 10, 11]],
        PO_5=[[3, 6, 7], [4, 5, 8]],
    ),
    PlayoffEnum.PO16: dict(
        PO_1=[[1, 8, 9, 16], [2, 7, 10, 15], [3, 6, 11, 14], [4, 5, 12, 13]],
        PO_2=[[1, 4, 5, 8], [2, 3, 6, 7], [9, 12, 13, 16], [10, 11, 14, 15]],
        PO_3=[[5, 8, 9, 12], [6, 7, 10, 11]],
    ),
}


class GroupCursors(NamedTuple):
    table_cursor: Cursor
    match_cursor: Cursor


def calc_group_points(cur):
    return f'=IF({cur.clone_shift(x=-1).as_cell()}="","",4-{cur.clone_shift(x=-1).as_cell()}+{cur.clone_shift(x=-2).as_cell()}/1000)'


def filter_protocols_by_column(cell, protocols, cursor):
    col = col_to_a(cursor.x)
    sums = []
    for pr in protocols:
        title = protocols[pr].ws.title
        sum_ = (
            f"SUM(IFNA(FILTER('{title}'!{col}1:{col}99, '{title}'!A1:A99 = {cell}), 0))"
        )
        sums.append(sum_)
    return "=" + " + ".join(sums)


def magic_header(heading, hidden=False):
    return [
        FCell(heading, Cf.ITALIC, hidden=hidden),
        FCell("Place", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Points", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Plus", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(50, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(40, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(30, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(20, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(10, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Toss", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
    ]


def group_header(hidden=False):
    return [
        FCell("Игрок", Cf.ITALIC, width=150, hidden=hidden),
        FCell("Очки", Cf.ITALIC + Cf.RIGHT, width=42, hidden=hidden),
        FCell("Круг 1", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 2", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 3", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 4", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
    ]


def get_place(match_cursors, place):
    pc = match_cursors.places_protocol_cursor.clone_shift(y=1)
    return f'=IFERROR(VLOOKUP({place}, {pc.as_cell_full()}:{pc.clone_shift(x=1,y=3).as_cell_full()}, 2, FALSE), "{match_cursors.match_id}-{place}")'


class GroupRange(NamedTuple):
    start: Cursor
    end: Cursor


class SpbOtborMaker(SiMaker):
    def add_po_1(self):
        po = self.po
        po_prot = self.po_prot
        cur = self.cur
        group_stage_finished = self.after_second_qual_round_checkbox
        po.cursor.add_block([FCell(None, width=34)])
        po.cursor.set(x=2, y=1)
        po.add_block(
            [FCell("1 этап", Cf.font_size(13))], horizontal=True, add_newline=True
        )
        po.cursor.shift(y=1)
        po_prot.cursor.add_block(
            [FCell("1 этап", Cf.font_size(13), height=40)], add_newline=True
        )
        cur.y = 26
        cur.x = 1
        cur.add_block(
            magic_header("PO-1", hidden=True),
            add_newline=True,
            horizontal=True,
        )
        if self.po_scheme == PlayoffEnum.PO24_2:
            round_scheme = PLAYOFF_COMPOSITION[PlayoffEnum.PO24_2]["PO_2"]
            n_green = 3
        else:
            round_scheme = PLAYOFF_COMPOSITION[PlayoffEnum.PO16]["PO_1"]
            n_green = 4
        total_participants = 0
        for i, match in enumerate(round_scheme):
            po.cursor.clone_shift(y=1, x=-1).add_block(match)
            total_participants += len(match)
            match_cursors = self.add_si_match(
                po.cursor,
                po_prot.cursor,
                [
                    f'=IF({group_stage_finished.as_cell_full()}, {self.after_second_qual_round.clone_shift(y=n - 1).as_cell_full()}, "Отбор-{n}")'
                    for n in match
                ],
                add_newline=True,
                table_bg_color=GREEN_BG if i < n_green else RED_BG,
                match_id_suffix=f" (пл. {int(i + 1)})",
                add_tiebreak=True,
            )
            places_cursor = match_cursors.places_cursor
            sum_plus_cursor = match_cursors.sum_plus_cursor
            for j in range(len(match)):
                p_num = j + 1
                cur.add_block(
                    [
                        "=" + places_cursor.clone_shift(y=p_num, x=-2).as_cell_full(),
                        f"={places_cursor.clone_shift(y=p_num):f} + {10 if i < n_green else 20}",
                        "=" + places_cursor.clone_shift(y=p_num, x=-1).as_cell_full(),
                    ]
                    + [
                        "=" + sum_plus_cursor.clone_shift(y=p_num, x=k).as_cell_full()
                        for k in range(6)
                    ],
                    add_newline=True,
                    horizontal=True,
                )
        self.sort_cur = self.magic.spawn_cursor(y=26, x=11)
        self.sort_cur.add_block(magic_header("PO-1"), add_newline=True, horizontal=True)
        self.after_prev = self.sort_cur.clone()
        self.sort_cur.add_block(
            [f"=SORT(A27:J{27 + total_participants - 1}, 2, 1, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"],
            add_newline=True,
        )
        self.sort_cur.y += 23

    def make_zh_reg(self):
        self.reg = self.get_worksheet("Регистрация", remove=True)
        init_worksheet(self.reg)
        cur = self.reg.cursor
        cur.freeze_row()
        cur.hblock_n(
            [
                FCell("Фамилия", Cf.BOLD, width=150),
                FCell("Имя", Cf.BOLD, width=90),
                FCell("Оплата (вовремя)", Cf.BOLD + Cf.CENTER, width=150),
                FCell("На месте", Cf.BOLD, width=80),
                FCell("Посев", Cf.BOLD + Cf.RIGHT, width=80),
            ]
        )
        range_start = cur.clone()
        players_num = self.players_num
        cur.shift(x=2).vblock([FCell(None, Cf.CENTER)] * players_num)
        cur.vblock([FCell(Checkbox(), Cf.CENTER)] * players_num)
        cur.vblock(list(range(1, players_num + 1)))
        cur.hblock_n(
            [FCell("Регистрация завершена", Cf.CENTER + Cf.BOLD, merge_shift_x=1), None]
        )
        reg_is_finished = cur.clone()
        cur.hblock([FCell(Checkbox(), Cf.CENTER, merge_shift_x=1), None])
        range_end = range_start.clone_shift(x=3, y=players_num - 1)
        is_present_range_start = range_start.clone_shift(x=3)
        is_present_range_end = is_present_range_start.clone_shift(y=players_num - 1)
        posev_range_start = range_start.clone_shift(x=4)
        posev_range_end = posev_range_start.clone_shift(y=players_num - 1)
        cur.shift(y=-1).hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({range_start}:{range_end}, """
                    f"""{is_present_range_start}:{is_present_range_end}, FALSE, """
                    f"""{posev_range_start}:{posev_range_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        second_range_start = cur.clone_shift(x=-1)
        second_range_end = second_range_start.clone_shift(x=1, y=players_num - 1)
        second_zh_start = second_range_start.clone_shift(x=4)
        second_zh_end = second_zh_start.clone_shift(y=players_num - 1)
        third_zh_start = second_zh_start.clone_shift(x=1)
        third_zh_end = third_zh_start.clone_shift(y=players_num - 1)
        cur.shift(x=3, y=-1)
        cur.hblock_n(
            [
                FCell("Жеребьёвка1", Cf.RIGHT),
                FCell("Жеребьёвка2", Cf.RIGHT),
            ]
        )
        if self.players_num in (36, 48):
            self.matches_in_round = 12
        else:
            self.matches_in_round = 8
        if self.players_num in (32, 48):
            self.players_in_group_match = 4
        elif self.players_num == 36:
            self.players_in_group_match = 3
        else:
            raise NotImplementedError
        groups = []
        for _ in range(self.players_in_group_match):
            group = list(range(1, self.matches_in_round + 1))
            self.rnd.shuffle(group)
            groups.extend(group)
        cur.vblock(groups)
        zher2 = []
        for _ in range(players_num):
            zher2.append(self.rnd.randint(1, 1000))
        cur.vblock(zher2)
        cur.shift(y=-1).hblock_n(
            [
                FCell("Фамилия", Cf.BOLD),
                FCell("Имя", Cf.BOLD),
                FCell("Группа", Cf.BOLD + Cf.RIGHT),
            ]
        )
        cur.hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({second_range_start}:{second_range_end}, """
                    f"""{second_zh_start}:{second_zh_end}, TRUE, """
                    f"""{third_zh_start}:{third_zh_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        self.init_qual_cells = {}
        cur.shift(x=1)
        for n in range(6):
            second_range_start.clone_shift(x=n).hide_col()
        for n in range(1, self.matches_in_round + 1):
            group_start = cur.clone()
            cur.vblock_n([n] * self.players_in_group_match)
            group_end = cur.clone_shift(y=-1)
            self.init_qual_cells[n] = GroupRange(start=group_start, end=group_end)

    def make_qual_stage(self, round_=1):
        prot = self.get_worksheet(f"Круг {round_} (протоколы)", remove=True)
        rnd = self.get_worksheet(f"Круг {round_}", remove=True)
        init_worksheet(prot)
        init_worksheet(rnd)
        rnd.spawn_cursor(x=1, y=1).hblock([FCell(None, width=25)])
        rnd.spawn_cursor(x=2, y=1).hblock(
            [FCell("Первый заход", Cf.font_size(15), height=50)]
        )
        rnd.spawn_cursor(x=6, y=1).hblock(
            [FCell("Второй заход", Cf.font_size(15), height=50)]
        )
        table_cur = rnd.spawn_cursor(x=10, y=1)
        if round_ == 1:
            table_cur_header = [
                FCell("Фамилия", Cf.ITALIC, width=150),
                FCell("Очки", Cf.ITALIC, width=50),
                FCell("М", Cf.ITALIC + Cf.RIGHT, width=28),
                FCell("Σ", Cf.ITALIC + Cf.RIGHT, width=40),
                FCell("Σ+", Cf.ITALIC + Cf.RIGHT, width=40),
            ]
        else:
            table_cur_header = [
                FCell("Фамилия", Cf.ITALIC, width=150),
                FCell("Очки 1+2", Cf.ITALIC, width=60),
                FCell("М 1+2", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Σ 1+2", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Σ+ 1+2", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Очки 1", Cf.ITALIC, width=55),
                FCell("М 1", Cf.ITALIC + Cf.RIGHT, width=32),
                FCell("Σ 1", Cf.ITALIC + Cf.RIGHT, width=45),
                FCell("Σ+ 1", Cf.ITALIC + Cf.RIGHT, width=45),
                FCell("Очки 2", Cf.ITALIC, width=55),
                FCell("М 2", Cf.ITALIC + Cf.RIGHT, width=32),
                FCell("Σ 2", Cf.ITALIC + Cf.RIGHT, width=45),
                FCell("Σ+ 2", Cf.ITALIC + Cf.RIGHT, width=45),
            ]
        table_cur.hblock_n(table_cur_header)
        table_cur.clone().hblock([FCell(None, hidden=True)] * len(table_cur_header))
        pointers = []
        matches_in_batch = self.matches_in_round // 2
        for i in range(1, self.matches_in_round + 1):
            pl_num = i % matches_in_batch or matches_in_batch
            if i >= matches_in_batch + 1:
                y_add = (i - (matches_in_batch + 1)) * 6
            else:
                y_add = (i - 1) * 6
            if round_ == 1:
                participants = [
                    "="
                    + self.init_qual_cells[i]
                    .start.clone_shift(y=x, x=-2)
                    .as_cell_full()
                    for x in range(self.players_in_group_match)
                ]
            else:
                if self.players_num == 48:
                    p_ids = [1 + (i - 1), 24 - (i - 1), 25 + (i - 1), 48 - (i - 1)]
                elif self.players_num == 32:
                    p_ids = [1 + (i - 1), 16 - (i - 1), 17 + (i - 1), 32 - (i - 1)]
                elif self.players_num == 36:
                    p_ids = QUAL_ROUND_2[i - 1]
                else:
                    raise NotImplementedError
                participants = [
                    f"=IF({self.after_first_qual_round_checkbox:f}, "
                    + f"{self.after_first_qual_round.clone_shift(y=p_ids[x] - 1):f} "
                    + f', "1 этап-{p_ids[x]}")'
                    for x in range(self.players_in_group_match)
                ]
            mc = self.add_si_match(
                table_cursor=rnd.spawn_cursor(x=2 + (0 if i < (matches_in_batch + 1) else 4), y=2 + y_add),
                protocol_cursor=prot.cursor,
                participants=participants,
                themes=10,
                italic_headline=True,
                match_id_suffix=f" (пл. {int(pl_num)})",
                table_bg_color=LIGHT_GRAY_BG,
            )
            for i, pl in enumerate(participants):
                if round_ == 1:
                    pointers.append(
                        table_cur.hblock_n(
                            [
                                pl,
                                lambda x: f"=IF({x.clone_shift(x=1)} > 0, 4-{x.clone_shift(x=1)}+{x.clone_shift(x=2)}/100, 0)",
                                "="
                                + mc.places_cursor.clone_shift(y=i + 1).as_cell_full(),
                                "="
                                + mc.sums_cursor.clone_shift(y=i + 1).as_cell_full(),
                                "="
                                + mc.sum_plus_cursor.clone_shift(
                                    y=i + 1
                                ).as_cell_full(),
                            ]
                        )
                    )
                else:
                    search_range = (
                        f"{self.first_qual_table_start:f}:{self.first_qual_table_end:f}"
                    )

                    def sum_func(x):
                        return f"={x.clone_shift(x=4)} + {x.clone_shift(x=8)}"

                    block = [
                        pl,  # player
                        sum_func,  # score1 + score2
                        sum_func,  # place1 + place2
                        sum_func,  # sum1 + sum2
                        sum_func,  # sumplus1 + sumplus2
                    ]
                    block.append(
                        lambda x: f"=VLOOKUP({x.clone_shift(x=-5)}, {search_range}, 2, FALSE)"
                    )  # score1
                    block.append(
                        lambda x: f"=VLOOKUP({x.clone_shift(x=-6)}, {search_range}, 3, FALSE)"
                    )  # place1
                    block.append(
                        lambda x: f"=VLOOKUP({x.clone_shift(x=-7)}, {search_range}, 4, FALSE)"
                    )  # sum1
                    block.append(
                        lambda x: f"=VLOOKUP({x.clone_shift(x=-8)}, {search_range}, 5, FALSE)"
                    )  # sumplus1
                    block.append(
                        lambda x: f"=IF({x.clone_shift(x=1)} > 0, 4-{x.clone_shift(x=1)}+{x.clone_shift(x=2)}/100, 0)"
                    )  # score2
                    block.append(
                        "=" + mc.places_cursor.clone_shift(y=i + 1).as_cell_full()
                    )  # place2
                    block.append(
                        "=" + mc.sums_cursor.clone_shift(y=i + 1).as_cell_full()
                    )  # sum2
                    block.append(
                        "=" + mc.sum_plus_cursor.clone_shift(y=i + 1).as_cell_full()
                    )  # sumplus2
                    pointers.append(table_cur.hblock_n(block))
        table_cur.set(y=1, x=pointers[-1].max_x + 1)
        table_cur.hblock_n(table_cur_header)
        table_start = rnd.spawn_cursor(x=pointers[0].min_x, y=pointers[0].min_y)
        if round_ == 1:
            self.first_qual_table_start = table_start
        table_end = rnd.spawn_cursor(x=pointers[-1].max_x, y=pointers[-1].max_y)
        if round_ == 1:
            self.first_qual_table_end = table_end
            self.after_first_qual_round = table_cur.clone()
        else:
            self.after_second_qual_round = table_cur.clone()
        if round_ == 1:
            table_cur.hblock(
                [
                    f"=SORT({table_start:f}:{table_end:f}, 2, FALSE, 3, TRUE, 4, FALSE, 5, FALSE)"
                ]
            )
        else:
            table_cur.hblock(
                [
                    f"""=IF({self.after_first_qual_round_checkbox:f}, SORT({table_start:f}:{table_end:f}, 2, FALSE, 3, TRUE, 4, FALSE, 5, FALSE), "Второй этап не начался")"""
                ]
            )
        table_cur.shift(x=pointers[-1].max_x - pointers[-1].min_x, y=-1).hblock_n(
            [FCell(f"{round_} круг закончен", Cf.CENTER, width=130)]
        )
        if round_ == 1:
            self.after_first_qual_round_checkbox = table_cur.clone()
        else:
            self.after_second_qual_round_checkbox = table_cur.clone()
        table_cur.hblock_n([FCell(Checkbox(), Cf.CENTER)])

    def make_qual(self):
        self.make_qual_stage(round_=1)
        self.make_qual_stage(round_=2)

    def make_si(self, players_num=48, po_scheme=PlayoffEnum.PO24_2):
        assert players_num in (32, 36, 48)
        self.players_num = players_num
        self.po_scheme = po_scheme
        self.match_counter = 1
        self.rnd = random.SystemRandom()
        self.make_zh_reg()
        self.plosch = self.get_worksheet("Площадки", remove=True)
        init_worksheet(self.plosch)
        self.plosch.hblock_n(
            [
                FCell("№", Cf.BOLD + Cf.RIGHT, width=40),
                FCell("Место", Cf.BOLD, width=100),
                FCell("Ведущий", Cf.BOLD, width=150),
                FCell("Ассистент", Cf.BOLD, width=150),
            ]
        )
        self.plosch.vblock(list(range(1, 7)))
        self.make_qual()
        self.po_prot = self.get_worksheet("Плей-офф (протоколы)", remove=True)
        self.po = self.get_worksheet("Плей-офф", remove=True)
        init_worksheet(self.po_prot)
        init_worksheet(self.po)
        self.magic = self.get_worksheet("Magic", remove=True)
        init_worksheet(self.magic)
        self.cur = self.magic.cursor
        self.sort_cur = self.magic.spawn_cursor(x=11, y=1)
        self.sort_cur.add_block(
            magic_header("Groups"), add_newline=True, horizontal=True
        )
        self.add_po_1()
        comp_dict = PLAYOFF_COMPOSITION[self.po_scheme]
        if self.po_scheme == PlayoffEnum.PO24_2:
            self.add_po_next(
                stage_num=2,
                hr_prev_stage_num="Первый",
                po_composition=comp_dict["PO_3"],
                n_green=2,
            )
            self.add_po_next(
                stage_num=3,
                hr_prev_stage_num="Второй",
                po_composition=comp_dict["PO_4"],
                n_green=1,
            )  # upper bracket final
            lower_bracket_semifinals = self.add_po_next(
                stage_num=4,
                hr_prev_stage_num="Третий",
                po_composition=comp_dict["PO_5"],
                n_green=0,
                skip_matches_for_stats=2,
            )
            self.add_po_wo_shuffle(
                matches=[
                    [
                        get_place(lower_bracket_semifinals[0], 1),
                        get_place(lower_bracket_semifinals[0], 2),
                        get_place(lower_bracket_semifinals[1], 1),
                        get_place(lower_bracket_semifinals[1], 2),
                    ]
                ],
                n_green=0,
                stage_name="Финал нижней сетки",
            )  # lower bracket final
        elif self.po_scheme == PlayoffEnum.PO16:
            second_round = self.add_po_next(
                stage_num=2,
                hr_prev_stage_num="Первый",
                po_composition=comp_dict["PO_2"],
                n_green=2,
            )
            third_round = self.add_po_next(
                stage_num=3,
                hr_prev_stage_num="Второй",
                po_composition=comp_dict["PO_3"],
                n_green=0,
            )
            fourth_round = self.add_po_wo_shuffle(
                matches=[
                    [
                        get_place(second_round[0], 1),
                        get_place(second_round[0], 2),
                        get_place(second_round[1], 1),
                        get_place(second_round[1], 2),
                    ],
                    [
                        get_place(third_round[0], 1),
                        get_place(third_round[0], 2),
                        get_place(third_round[1], 1),
                        get_place(third_round[1], 2),
                    ],
                ],
                n_green=1,
                stage_name="4 этап",
            )
            self.add_po_wo_shuffle(
                matches=[
                    [
                        get_place(fourth_round[0], 3),
                        get_place(fourth_round[0], 4),
                        get_place(fourth_round[1], 1),
                        get_place(fourth_round[1], 2),
                    ],
                ],
                n_green=1,
                stage_name="Финал нижней сетки",
            )
        else:
            raise NotImplementedError
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    parser.add_argument("--players_num", "-pn", type=int, default=48)
    parser.add_argument(
        "--po_scheme",
        "-ps",
        type=enumtype,
        default=PlayoffEnum.PO24_2.name,
    )
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = SpbOtborMaker(gc, args.spreadsheet_id)
    writer.make_si(players_num=args.players_num, po_scheme=args.po_scheme)


if __name__ == "__main__":
    main()
