from collections import defaultdict
from typing import Callable, NamedTuple

from google_sheet_writer import (
    Cf,
    Checkbox,
    Cursor,
    FCell,
    GoogleSheetWriter,
    col_to_a,
    make_address,
)
from gspread_formatting import (
    BooleanCondition,
    BooleanRule,
    CellFormat,
    Color,
    ColorStyle,
    ConditionalFormatRule,
    DataValidationRule,
    GridRange,
    TextFormat,
)

from studchr_tables_common.utils import (
    DARKER_GRAY,
    GRAY,
    LIGHT_GRAY,
    GREEN,
    LIGHT_GRAY_BG,
    RED,
)
from gspread_formatting import (
    CellFormat,
    border,
    borders,
)


ALL_BORDERS = CellFormat(
    borders=borders(
        bottom=border("SOLID", LIGHT_GRAY),
        top=border("SOLID", LIGHT_GRAY),
        left=border("SOLID", LIGHT_GRAY),
        right=border("SOLID", LIGHT_GRAY),
    )
)


def get_red_green_rules(cursor1, cursor2):
    a1_range = f"{cursor1.as_cell()}:{cursor2.as_cell()}"
    return [
        ConditionalFormatRule(
            ranges=[GridRange.from_a1_range(a1_range, cursor1.wks.ws)],
            booleanRule=BooleanRule(
                condition=BooleanCondition("NUMBER_EQ", ["1"]),
                format=CellFormat(
                    textFormat=TextFormat(
                        foregroundColorStyle=ColorStyle(rgbColor=GREEN)
                    ),
                    backgroundColor=GREEN,
                ),
            ),
        ),
        ConditionalFormatRule(
            ranges=[GridRange.from_a1_range(a1_range, cursor1.wks.ws)],
            booleanRule=BooleanRule(
                condition=BooleanCondition("NUMBER_EQ", ["0"]),
                format=CellFormat(
                    textFormat=TextFormat(
                        foregroundColorStyle=ColorStyle(rgbColor=RED)
                    ),
                    backgroundColor=RED,
                ),
            ),
        ),
    ]


class MatchCursors(NamedTuple):
    init_cursor: Cursor
    result_formula: Cursor
    reverse_result_formula: Cursor
    first_free_row: Cursor
    group_points1: str
    group_points2: str
    questions1: str
    questions2: str
    questions1_wo_tiebreak: str
    questions2_wo_tiebreak: str
    winner_formula: str
    loser_formula: str
    match_id: str
    participant1: str
    participant2: str
    match_is_finished: str


class GroupCursors(NamedTuple):
    init_cursor: Cursor
    team_cursor: Cursor
    places_cursor: Cursor
    match_cursors: list[MatchCursors]
    first_free_row: Cursor
    n_participants: int
    get_place_formula: Callable[[int], str]


class DECursors(NamedTuple):
    init_cursor: Cursor
    match_cursors: list[MatchCursors]
    get_place_formula: Callable[[int], str]
    group_id: str


class KinsbfMaker(GoogleSheetWriter):
    GROUP_MATCHES = {
        4: [
            [[1, 2], [3, 4]],
            [[1, 4], [2, 3]],
            [[1, 3], [2, 4]],
        ],
        3: [[[1, 2]], [[1, 3]], [[2, 3]]],
    }
    PF_GROUPS = [[1, 8, 9], [2, 7, 10], [3, 6, 11], [4, 5, 12]]

    def get_dv_rule(self, cursor):
        return DataValidationRule(
            BooleanCondition(
                "ONE_OF_RANGE",
                [f"={cursor.as_cell()}:{cursor.clone_shift(y=8).as_cell()}"],
            ),
            showCustomUi=True,
        )

    def get_sost_formula(self, cursor):
        return f"=FILTER('Составы'!B1:B500, 'Составы'!A1:A500 = {cursor.as_cell()})"

    def get_group_points(self, participant_1_sum, participant_2_sum):
        return f"""IF({participant_1_sum} > {participant_2_sum}, 2, IF({participant_1_sum} = {participant_2_sum}, 1, 0))"""

    def add_match(
        self,
        participants,
        protocol_cursor,
        n_questions=5,
        add_tiebreak=False,
        stats_for_reshuffle=False,
    ):
        init_cursor = protocol_cursor.clone()
        match_id = col_to_a(self.match_counter)
        match_name = f"Бой {match_id}"
        protocol_cursor.set_row_height(55)
        block = []
        for i in range(1, n_questions + 1):
            block.append(FCell(i, Cf.CENTER))
        if add_tiebreak:
            block.append(FCell("П", Cf.CENTER))
        protocol_cursor.clone_shift(y=1).add_block(block)
        n_shift = n_questions + (1 if add_tiebreak else 0)

        def end(x):
            return x.clone_shift(y=n_shift - 1)

        participant_1_players = protocol_cursor.clone_shift(x=1, y=1)
        participant_1_points = participant_1_players.clone_shift(x=1)
        participant_2_points = participant_1_players.clone_shift(x=2)
        participant_2_players = participant_1_players.clone_shift(x=3)
        ind_stats = []
        for i in range(n_questions):
            ind_stats.append(
                [
                    match_name,
                    participants[0],
                    "=" + participant_1_players.clone_shift(y=i).as_cell_full(),
                    "=" + participant_1_points.clone_shift(y=i).as_cell_full(),
                ]
            )
            ind_stats.append(
                [
                    match_name,
                    participants[1],
                    "=" + participant_2_players.clone_shift(y=i).as_cell_full(),
                    "=" + participant_2_points.clone_shift(y=i).as_cell_full(),
                ]
            )
        participant_1_sum = f"""SUM({participant_1_points.as_cell_full()}:{end(participant_1_points).as_cell_full()})"""
        participant_2_sum = f"""SUM({participant_2_points.as_cell_full()}:{end(participant_2_points).as_cell_full()})"""
        for pp in (participant_1_points, participant_2_points):
            pp.clone().vblock([FCell(None, ALL_BORDERS)] * n_shift)
        if add_tiebreak:
            participant_1_sum_for_stats = f"""SUM({participant_1_points.as_cell_full()}:{end(participant_1_points).shift(y=-1).as_cell_full()})"""
            participant_2_sum_for_stats = f"""SUM({participant_2_points.as_cell_full()}:{end(participant_2_points).shift(y=-1).as_cell_full()})"""
        else:
            participant_1_sum_for_stats = participant_1_sum
            participant_2_sum_for_stats = participant_2_sum
        match_count_formula = f"""={participant_1_sum}&" : "&{participant_2_sum}"""
        reverse_match_count_formula = (
            f"""={participant_2_sum}&" : "&{participant_1_sum}"""
        )
        participant_1_points.wks.add_to_conditional_formatting(
            get_red_green_rules(participant_1_points, end(participant_1_points))
        )
        participant_1_points.wks.add_to_conditional_formatting(
            get_red_green_rules(participant_2_points, end(participant_2_points))
        )
        for_team_names = protocol_cursor.clone()
        protocol_cursor.add_block(
            [
                FCell(
                    match_id,
                    Cf.text_color(DARKER_GRAY) + LIGHT_GRAY_BG + Cf.MIDDLE + Cf.CENTER,
                    width=30,
                ),
                FCell(participants[0], Cf.WRAP + LIGHT_GRAY_BG + Cf.MIDDLE, width=150),
                FCell(
                    match_count_formula,
                    LIGHT_GRAY_BG + Cf.CENTER + Cf.MIDDLE,
                    merge_shift_x=1,
                    width=21,
                ),
                FCell(None, width=21),
                FCell(participants[1], LIGHT_GRAY_BG + Cf.WRAP + Cf.MIDDLE, width=150),
            ],
            horizontal=True,
        )
        team_name_1 = for_team_names.clone_shift(x=1)
        team_name_2 = for_team_names.clone_shift(x=4)
        sost_cursor_1 = end(participant_1_players).clone_shift(y=1)
        sost_cursor_2 = end(participant_2_players).clone_shift(y=1)
        dv_rule_1 = self.get_dv_rule(sost_cursor_1)
        dv_rule_2 = self.get_dv_rule(sost_cursor_2)
        sost_formula_1 = self.get_sost_formula(team_name_1)
        sost_formula_2 = self.get_sost_formula(team_name_2)
        first_free_row_cursor = sost_cursor_1.clone()
        first_free_row_cursor.add_block(
            [FCell(sost_formula_1, hidden_row=True)]
            + [FCell(None, hidden_row=True)] * 9,
            add_newline=True,
        )
        sost_cursor_2.clone().add_block(
            [FCell(sost_formula_2, hidden_row=True)]
            + [FCell(None, hidden_row=True)] * 9
        )
        participant_1_players.clone().add_block(
            [FCell(None, dv_rule=dv_rule_1)] * n_shift
        )
        participant_2_players.clone().add_block(
            [FCell(None, dv_rule=dv_rule_2)] * n_shift
        )
        points1 = self.get_group_points(participant_1_sum, participant_2_sum)
        points2 = self.get_group_points(participant_2_sum, participant_1_sum)
        protocol_cursor.set(y=init_cursor.y, x=init_cursor.x + 5)
        if add_tiebreak:
            checkbox = protocol_cursor.clone()
            ch_path = checkbox.as_cell_full()
            protocol_cursor.add_block(
                [
                    FCell(Checkbox(), width=32),
                    FCell(None, width=32),
                ],
                horizontal=True,
            )
            winner_formula = f"""IF(AND({ch_path}, {participant_1_sum} > {participant_2_sum}), {team_name_1.as_cell_full()}, IF(AND({ch_path}, {participant_2_sum} > {participant_1_sum}), {team_name_2.as_cell_full()}, "{match_id}-1"))"""
            loser_formula = f"""IF(AND({ch_path}, {participant_1_sum} > {participant_2_sum}), {team_name_2.as_cell_full()}, IF(AND({ch_path}, {participant_2_sum} > {participant_1_sum}), {team_name_1.as_cell_full()}, "{match_id}-2"))"""
        else:
            protocol_cursor.add_block([FCell(None, width=40)], horizontal=True)
            winner_formula = f"""IF({participant_1_sum} > {participant_2_sum}, {team_name_1.as_cell_full()}, IF({participant_2_sum} > {participant_1_sum}, {team_name_2.as_cell_full()}, "{match_id}-1"))"""
            loser_formula = f"""IF({participant_1_sum} > {participant_2_sum}, {team_name_2.as_cell_full()}, IF({participant_2_sum} > {participant_1_sum}, {team_name_1.as_cell_full()}, "{match_id}-2"))"""
            ch_path = ""
        if stats_for_reshuffle:
            self.for_reshuffle.add_block(
                [
                    match_id,
                    "=" + team_name_1.as_cell_full(),
                    "=" + participant_1_sum_for_stats,
                    n_questions,
                    f"=({participant_1_sum_for_stats} - {participant_2_sum_for_stats})",
                    "=" + points1,
                ],
                horizontal=True,
                add_newline=True,
            )
            self.for_reshuffle.add_block(
                [
                    match_id,
                    "=" + team_name_2.as_cell_full(),
                    "=" + participant_2_sum_for_stats,
                    n_questions,
                    f"=({participant_2_sum_for_stats} - {participant_1_sum_for_stats})",
                    "=" + points2,
                ],
                horizontal=True,
                add_newline=True,
            )
        self.match_counter += 1
        for block in ind_stats:
            self.ind_stats.hblock_n(block + ["=" + winner_formula])
        return MatchCursors(
            init_cursor,
            match_count_formula,
            reverse_match_count_formula,
            first_free_row_cursor,
            points1,
            points2,
            participant_1_sum,
            participant_2_sum,
            participant_1_sum_for_stats,
            participant_2_sum_for_stats,
            winner_formula,
            loser_formula,
            match_id,
            team_name_1.as_cell_full(),
            team_name_2.as_cell_full(),
            ch_path,
        )

    @classmethod
    def new_group_stat(cls):
        return {
            "group_points": [],
            "zab": [],
            "prop": [],
            "razn": [],
        }

    def add_group(
        self,
        participants,
        table_cursor,
        protocol_cursor,
        setka_cursor,
        suffix=None,
        stats_for_reshuffle=False,
    ):
        init_cursor = table_cursor.clone()
        group_id = col_to_a(self.group_counter)
        group_name = f"Группа {group_id}{suffix or ''}"
        table_cursor.add_block(
            [
                FCell(
                    group_name,
                    LIGHT_GRAY_BG + Cf.CENTER,
                    merge_shift_x=6 + len(participants),
                )
            ]
            + [FCell(None, LIGHT_GRAY_BG)] * (6 + len(participants)),
            horizontal=True,
            add_newline=True,
        )
        table_cursor.add_block(
            [
                FCell("№", Cf.CENTER + Cf.ITALIC, width=20),
                FCell("Команда", Cf.ITALIC, width=200),
            ]
            + [
                FCell(i + 1, Cf.CENTER + Cf.ITALIC, width=40)
                for i in range(len(participants))
            ]
            + [
                FCell("О", Cf.CENTER + Cf.ITALIC, width=32),
                FCell("'+", Cf.CENTER + Cf.ITALIC, width=32),
                FCell("−", Cf.CENTER + Cf.ITALIC, width=32),
                FCell("'+/−", Cf.CENTER + Cf.ITALIC, width=32),
                FCell("М", Cf.CENTER + Cf.ITALIC, width=32),
            ],
            horizontal=True,
            add_newline=True,
        )
        bc = table_cursor.add_block(
            [FCell(i + 1, Cf.CENTER + Cf.ITALIC) for i in range(len(participants))]
        )
        first_free_row = table_cursor.clone().set(x=bc.min_x, y=bc.max_y + 1)
        team_cursor = table_cursor.clone()
        table_cursor.add_block(participants)
        xcursor = table_cursor.clone()
        for _ in range(len(participants)):
            xcursor.add_block([FCell("×", Cf.text_color(GRAY) + Cf.CENTER)])
            xcursor.shift(y=1)
        group_stats = defaultdict(lambda: self.new_group_stat())
        match_cursors = []
        protocol_cursor.add_block(
            [FCell(group_name, Cf.ITALIC, height=36)], add_newline=True
        )
        for round in self.GROUP_MATCHES[len(participants)]:
            for match in round:
                part1 = participants[match[0] - 1]
                part2 = participants[match[1] - 1]
                mc = self.add_match(
                    [part1, part2],
                    protocol_cursor,
                    stats_for_reshuffle=stats_for_reshuffle,
                )
                match_cursors.append(mc)
                group_stats[part1]["group_points"].append(mc.group_points1)
                group_stats[part1]["zab"].append(mc.questions1)
                group_stats[part1]["prop"].append(mc.questions2)
                group_stats[part1]["razn"].append(
                    f"({mc.questions1} - {mc.questions2})"
                )
                group_stats[part2]["group_points"].append(mc.group_points2)
                group_stats[part2]["zab"].append(mc.questions2)
                group_stats[part2]["prop"].append(mc.questions1)
                group_stats[part2]["razn"].append(
                    f"({mc.questions2} - {mc.questions1})"
                )
                team_cursor.clone_shift(y=match[0] - 1, x=match[1]).add_block(
                    [FCell(mc.result_formula, Cf.CENTER)]
                )
                team_cursor.clone_shift(y=match[1] - 1, x=match[0]).add_block(
                    [FCell(mc.reverse_result_formula, Cf.CENTER)]
                )
        stats_cursor = team_cursor.clone_shift(x=len(participants) + 1)
        for key in ("group_points", "zab", "prop", "razn"):
            stats_cursor.add_block(
                [
                    FCell("=" + " + ".join(group_stats[p][key]), Cf.CENTER)
                    for p in participants
                ]
            )
        places_cursor = stats_cursor.clone()
        stats_cursor.clone().add_block([FCell(None, Cf.CENTER)] * len(participants))
        protocol_cursor.set(
            x=match_cursors[0].init_cursor.x, y=match_cursors[0].first_free_row.y
        )
        init_setka_cursor = setka_cursor.clone()
        setka_cursor.add_block(
            [
                FCell(group_name, LIGHT_GRAY_BG, width=200),
                FCell("М", Cf.RIGHT + LIGHT_GRAY_BG, width=32),
            ],
            horizontal=True,
            add_newline=True,
        )
        setka_cursor.add_block(participants)
        setka_cursor.add_block(
            [
                "=" + places_cursor.clone_shift(y=i).as_cell_full()
                for i in range(len(participants))
            ]
        )
        setka_cursor.set(x=init_setka_cursor.x, y=init_setka_cursor.y + 6)
        init_cursor.clone_shift(x=11).add_block([FCell(None, width=40)])

        def get_place_formula_maker(group_id):
            def get_place_formula(n):
                places_range = (
                    places_cursor.as_cell_full()
                    + ":"
                    + places_cursor.clone_shift(y=len(participants) - 1).as_cell_full()
                )
                teams_range = (
                    team_cursor.as_cell_full()
                    + ":"
                    + team_cursor.clone_shift(y=len(participants) - 1).as_cell_full()
                )
                place_match = f"MATCH({n}, {places_range}, 0)"
                return f"""IFERROR(INDEX({teams_range}, {place_match}), "Гр. {group_id}-{n}")"""

            return get_place_formula

        self.group_counter += 1
        return GroupCursors(
            init_cursor,
            team_cursor,
            places_cursor,
            match_cursors,
            first_free_row,
            len(participants),
            get_place_formula_maker(group_id),
        )

    def add_table_de_match(
        self,
        match_cursors: MatchCursors,
        table_cursor: Cursor,
        color: Color = LIGHT_GRAY,
    ):
        table_cursor.add_block(
            [
                FCell(
                    f"Бой {match_cursors.match_id}",
                    Cf.background_color(color),
                    width=200,
                ),
                FCell("Счёт", Cf.RIGHT + Cf.background_color(color), width=50),
            ],
            horizontal=True,
            add_newline=True,
        )
        table_cursor.add_block(
            ["=" + match_cursors.participant1, "=" + match_cursors.questions1],
            horizontal=True,
            add_newline=True,
        )
        table_cursor.add_block(
            ["=" + match_cursors.participant2, "=" + match_cursors.questions2],
            horizontal=True,
            add_newline=True,
        )

    def add_de(
        self,
        participants,
        table_cursor,
        protocol_cursor,
        setka_cursor,
        suffix=None,
        stats_for_reshuffle=False,
    ):
        assert len(participants) == 4
        init_cursor = table_cursor.clone()
        group_id = col_to_a(self.group_counter)
        group_name = f"Группа {group_id}{suffix or ''}"
        match_cursors = []
        protocol_cursor.add_block(
            [FCell(group_name, Cf.ITALIC, height=36)], add_newline=True
        )
        table_cursor.add_block(
            [FCell(group_name, Cf.ITALIC, height=36)], add_newline=True
        )
        match_cursors.append(
            self.add_match(
                [participants[0], participants[1]],
                protocol_cursor,
                add_tiebreak=True,
                stats_for_reshuffle=stats_for_reshuffle,
            )
        )
        match_cursors.append(
            self.add_match(
                [participants[2], participants[3]],
                protocol_cursor,
                add_tiebreak=True,
                stats_for_reshuffle=stats_for_reshuffle,
            )
        )
        table_anchor = table_cursor.clone()
        self.add_table_de_match(match_cursors[0], table_cursor, color=GREEN)
        table_cursor.shift(y=2)
        self.add_table_de_match(match_cursors[1], table_cursor, color=GREEN)
        table_cursor.set(y=table_anchor.y, x=table_cursor.x + 2)
        table_cursor.add_block([FCell(None, width=40)])
        match_cursors.append(
            self.add_match(
                [
                    "=" + match_cursors[0].winner_formula,
                    "=" + match_cursors[1].winner_formula,
                ],
                protocol_cursor,
                add_tiebreak=True,
                stats_for_reshuffle=stats_for_reshuffle,
            )
        )
        match_cursors.append(
            self.add_match(
                [
                    "=" + match_cursors[0].loser_formula,
                    "=" + match_cursors[1].loser_formula,
                ],
                protocol_cursor,
                add_tiebreak=True,
                stats_for_reshuffle=stats_for_reshuffle,
            )
        )
        table_anchor = table_cursor.clone()
        self.add_table_de_match(match_cursors[2], table_cursor, color=GREEN)
        table_cursor.shift(y=2)
        self.add_table_de_match(match_cursors[3], table_cursor, color=RED)
        table_cursor.set(y=table_anchor.y, x=table_cursor.x + 2)
        table_cursor.add_block([FCell("→", Cf.font_size(12) + Cf.CENTER, width=40)])
        table_cursor.add_block(
            [
                FCell("Предфинальные группы", Cf.ITALIC),
                "=" + match_cursors[2].winner_formula,
            ],
            add_newline=True,
        )
        table_cursor.shift(y=3)
        match_cursors.append(
            self.add_match(
                [
                    "=" + match_cursors[2].loser_formula,
                    "=" + match_cursors[3].winner_formula,
                ],
                protocol_cursor,
                add_tiebreak=True,
                stats_for_reshuffle=stats_for_reshuffle,
            )
        )
        protocol_cursor.set(y=match_cursors[0].first_free_row.y, x=2)
        table_anchor = table_cursor.clone()
        self.add_table_de_match(match_cursors[4], table_cursor, color=RED)
        table_cursor.set(y=table_anchor.y, x=table_cursor.x + 2)
        table_cursor.add_block([FCell("→", Cf.font_size(12) + Cf.CENTER, width=40)])
        table_cursor.add_block(
            [
                FCell("Предфинальные группы", Cf.ITALIC),
                "=" + match_cursors[4].winner_formula,
            ],
            add_newline=True,
        )
        table_cursor.set(x=init_cursor.x, y=init_cursor.y + 10)
        setka_cursor.add_block(
            [
                FCell(group_name, LIGHT_GRAY_BG, width=200),
                FCell("М", LIGHT_GRAY_BG + Cf.RIGHT, width=32),
            ],
            horizontal=True,
            add_newline=True,
        )

        def get_place_formula(n):
            return {
                1: f"""IF({match_cursors[2].match_is_finished}, {match_cursors[2].winner_formula}, "Гр. {group_id}-1")""",
                2: f"""IF({match_cursors[4].match_is_finished}, {match_cursors[4].winner_formula}, "Гр. {group_id}-2")""",
                3: f"""IF({match_cursors[4].match_is_finished}, {match_cursors[4].loser_formula}, "Гр. {group_id}-3")""",
                4: f"""IF({match_cursors[3].match_is_finished}, {match_cursors[3].loser_formula}, "Гр. {group_id}-4")""",
            }[n]
        
        group_vlookup = setka_cursor.clone_shift(x=2)
        group_vlookup_start = group_vlookup.clone()
        for i in range(1, 5):
            group_vlookup.hblock_n(
                [FCell("=" + get_place_formula(i), hidden=True), FCell(i, hidden=True)]
            )
        group_vlookup_end = group_vlookup.clone_shift(y=-1, x=1)

        for i in range(1, 5):
            setka_cursor.add_block(
                [
                    participants[i - 1],
                    lambda x: f"=IFERROR(VLOOKUP({x.clone_shift(x=-1)}, {group_vlookup_start:p}:{group_vlookup_end:p}, 2, FALSE), \"\")"
                ],
                horizontal=True,
                add_newline=True,
            )
        setka_cursor.shift(y=1)
        self.group_counter += 1
        return DECursors(init_cursor, match_cursors, get_place_formula, group_id)

    def init_participant(self, basket, participant):
        return f"""=IF({self.zh_finished.as_cell_full()}, {self.venues_to_cursors[basket].clone_shift(y=participant).as_cell_full()}, "Корзина {basket}-{participant + 1}")"""
