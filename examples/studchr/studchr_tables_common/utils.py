from google_sheet_writer import (
    Cf,
    col_to_a,
    color_from_hex,
)
from gspread_formatting import (
    CellFormat,
    border,
    borders,
)

RANGE_ALL = "A1:CZ200"
RED = color_from_hex("f4c3c9")
GREEN = color_from_hex("52aa7f")
WHITE = color_from_hex("ffffff")
GREEN_BG = CellFormat(backgroundColor=GREEN)
RED_BG = CellFormat(backgroundColor=RED)
LIGHT_GRAY = color_from_hex("efefef")
GRAY = color_from_hex("d9d9d9")
DARKER_GRAY = color_from_hex("b7b7b7")
LIGHT_GRAY_BG = CellFormat(backgroundColor=LIGHT_GRAY)
LIGHT_GREEN_BG = CellFormat(backgroundColor=color_from_hex("88e3b7"))
BOTTOM_BORDER = CellFormat(borders=borders(bottom=border("DASHED", GRAY)))
ALL_BORDERS = CellFormat(
    borders=borders(
        bottom=border("DASHED"),
        top=border("DASHED"),
        left=border("DASHED"),
        right=border("DASHED"),
    )
)
BR_BORDERS = CellFormat(
    borders=borders(
        bottom=border("DASHED", GRAY),
        right=border("DASHED", GRAY),
    )
)
NON_TOP_BORDERS = CellFormat(
    borders=borders(
        bottom=border("DASHED", GRAY),
        left=border("DASHED", GRAY),
        right=border("DASHED", GRAY),
    )
)


def init_worksheet(wks):
    def _init_worksheet(wks, max_row=0, max_col=0):
        range_all = f"A1:{col_to_a(max_col)}:{max_row}"
        wks.hide_gridlines()
        wks.set_cell_format(
            range_all,
            Cf.font_family("Noto Sans") + Cf.font_size(11) + Cf.background_color(WHITE),
        )
        for i in range(1, max_row):
            rh_dict = wks.parent.fmt[wks.ws.title]["row_heights"]
            if i not in rh_dict:
                rh_dict[i] = 23
        wks.add_to_batch_clear(range_all)

    wks.init_format = _init_worksheet
