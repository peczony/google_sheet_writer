#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import NamedTuple

from google_sheet_writer import (
    Cf,
    Cursor,
    FCell,
    GoogleSheetWriter,
    col_to_a,
    make_address,
)
from gspread_formatting import (
    BooleanCondition,
    DataValidationRule,
)

from studchr_tables_common.si import get_red_green_rules, is_right, is_wrong
from studchr_tables_common.utils import (
    BOTTOM_BORDER,
    BR_BORDERS,
    LIGHT_GRAY_BG,
    NON_TOP_BORDERS,
)


class GroupCursors(NamedTuple):
    table_cursor: Cursor
    match_cursor: Cursor


class MatchCursors(NamedTuple):
    sums_cursor: Cursor
    places_cursor: Cursor
    places_protocol_cursor: Cursor
    sum_plus_cursor: Cursor
    match_id: str | int


class ThemeSumFunc:
    def __init__(self, participant_num):
        self.p = participant_num

    def __call__(self, cur):
        vals = []
        for i in range(1, 6):
            cell = cur.clone_shift(x=-6 + i, y=1)
            val_cell = cell.clone_shift(y=-(2 + self.p * 2))
            vals.append(
                f"IF({is_right(cell)}, {val_cell:pr}, IF({is_wrong(cell)}, -{val_cell:pr}, 0))"
            )
        return "=" + " + ".join(vals)


class SumPlus:
    def __init__(self, theme_metadata, participant_num):
        self.theme_metadata = theme_metadata
        self.p = participant_num

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            for n in (10, 20, 30, 40, 50):
                xcur = dct[n]
                cell = cur.replace(x=xcur.x, y=cur.y + 1)
                vals.append(
                    f"IF({is_right(cell.as_cell())}, {cell.clone_shift(y=-(2 + self.p * 2)).as_cell()}, 0)"
                )
        return "=" + " + ".join(vals)


class SumTotal:
    def __init__(self, theme_metadata):
        self.theme_metadata = theme_metadata

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            sum_col = dct["sum"]
            cell = cur.replace(x=sum_col.x)
            vals.append(cell.as_cell())
        return "=" + " + ".join(vals)


class CountValAnswers:
    def __init__(self, theme_metadata, val):
        self.theme_metadata = theme_metadata
        self.val = val

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            xcur = dct[self.val]
            cell = cur.replace(x=xcur.x, y=cur.y + 1)
            vals.append(f"IF({is_right(cell.as_cell())}, 1, 0)")
        return "=" + " + ".join(vals)


def calc_group_points(cur):
    return f'=IF({cur.clone_shift(x=-1).as_cell()}="","",4-{cur.clone_shift(x=-1).as_cell()}+{cur.clone_shift(x=-2).as_cell()}/1000)'


def filter_protocols_by_column(cell, protocols, cursor):
    col = col_to_a(cursor.x)
    sums = []
    for pr in protocols:
        title = protocols[pr].ws.title
        sum_ = (
            f"SUM(IFNA(FILTER('{title}'!{col}1:{col}99, '{title}'!A1:A99 = {cell}), 0))"
        )
        sums.append(sum_)
    return "=" + " + ".join(sums)


def get_place(match_cursors, place):
    pc = match_cursors.places_protocol_cursor.clone_shift(y=1)
    return f'=IFERROR(VLOOKUP({place}, {pc.as_cell_full()}:{pc.clone_shift(x=1,y=7).as_cell_full()}, 2, FALSE), "{match_cursors.match_id}-{place}")'


class EkMaker(GoogleSheetWriter):
    def add_ek_theme(
        self,
        protocol_cursor,
        theme_num,
        sost_cur,
        n_participants=3,
        match_id=None,
        first=False,
    ):
        metadata = {}
        players_cursor = protocol_cursor.clone_shift(y=1)
        for i in range(n_participants):

            def dv_rule_func(x):
                return DataValidationRule(
                    BooleanCondition(
                        "ONE_OF_RANGE",
                        [
                            f"={x.replace(x=sost_cur.x).as_cell()}:{x.replace(x=sost_cur.x+8).as_cell()}"
                        ],
                    ),
                    showCustomUi=True,
                )

            pl_ref = players_cursor.clone()
            players_cursor.add_block(
                [FCell(None, Cf.LEFT, dv_rule=dv_rule_func, merge_shift_x=4)]
                + [None] * 4,
                horizontal=True,
                add_newline=True,
            )
            self.ind_stats.add_block(
                [
                    f"Бой {match_id}",
                    f"Тема {theme_num}",
                    "=" + pl_ref.replace(x=1).as_cell_full(),
                    "=" + pl_ref.as_cell_full(),
                    "=" + pl_ref.clone_shift(x=5).as_cell_full(),
                ],
                horizontal=True,
                add_newline=True,
            )
            range_start = players_cursor.clone()
            range_end = players_cursor.clone_shift(x=4)
            protocol_cursor.wks.add_to_conditional_formatting(
                get_red_green_rules(protocol_cursor.wks, range_start, range_end)
            )
            players_cursor.add_block(
                [
                    FCell(None, BR_BORDERS if first else NON_TOP_BORDERS),
                    FCell(None, NON_TOP_BORDERS),
                    FCell(None, NON_TOP_BORDERS),
                    FCell(None, NON_TOP_BORDERS),
                    FCell(None, NON_TOP_BORDERS),
                ],
                horizontal=True,
                add_newline=True,
            )
        for i in range(1, 6):
            theme_val = i * 10
            metadata[theme_val] = protocol_cursor.clone()
            protocol_cursor.set_col_width(35)
            protocol_cursor.add_block([FCell(theme_val, Cf.CENTER + LIGHT_GRAY_BG)])
        metadata["sum"] = protocol_cursor.clone()
        protocol_cursor.set_col_width(40)
        protocol_cursor.align_column("CENTER")
        block = [FCell(f"Т{theme_num}", LIGHT_GRAY_BG)]
        for i in range(n_participants):
            block.extend(
                [
                    FCell(ThemeSumFunc(i), BOTTOM_BORDER, merge_shift_y=1),
                    None,
                ]
            )
        protocol_cursor.add_block(block)
        protocol_cursor.set_col_width(35)
        protocol_cursor.add_block(
            [FCell(None, LIGHT_GRAY_BG)]
            + [None, FCell(None, BOTTOM_BORDER)] * n_participants
        )
        return metadata

    def add_ek_match(
        self,
        table_cursor,
        protocol_cursor,
        participants,
        match_id=None,
        add_newline=False,
    ):
        init_cursor = protocol_cursor.clone()
        protocol_cursor.set_col_width(185)
        block = [FCell(f"Бой {match_id}", LIGHT_GRAY_BG, height=28)]
        for p in participants:
            block.extend(
                [
                    FCell(p, Cf.WRAP + BOTTOM_BORDER, merge_shift_y=1, height=33),
                    None,
                ]
            )
        sost_blocks = []
        for _ in range(len(participants)):
            sost_blocks.append(
                [
                    FCell(lambda c: f"=FILTER('Составы'!A2:QQ2, 'Составы'!A1:QQ1 = {c.replace(x=1).as_cell()})", hidden=True)
                ]
                + [FCell(None, hidden=True)] * 9
            )
            sost_blocks.append([None] * 9)
        sost_cur = protocol_cursor.clone_shift(x=95, y=1)
        for sost_block in sost_blocks:
            sost_cur.add_block(sost_block, horizontal=True, add_newline=True)
        protocol_cursor.add_block(block)
        cursor_for_sums = protocol_cursor.clone()
        protocol_cursor.x += 1
        protocol_cursor.set_col_width(50)
        cursor_for_places = protocol_cursor.clone()
        protocol_cursor.freeze_col()
        protocol_cursor.x += 1
        protocol_cursor.hide_col()
        protocol_cursor.add_block(
            ["Команда_hidden"]
            + [lambda x: "=" + x.clone_shift(x=-3).as_cell()] * len(participants) * 2
        )
        theme_metadata = {}
        for i in range(12):
            theme_num = i + 1
            theme_metadata[theme_num] = self.add_ek_theme(
                protocol_cursor,
                theme_num,
                sost_cur,
                n_participants=len(participants),
                match_id=match_id,
                first=i == 0,
            )

        protocol_cursor.vblock(
            [FCell("П", LIGHT_GRAY_BG, align="RIGHT", width=40)]
            + [FCell(0, merge_shift_y=1), None] * len(participants)
        )
        sum_plus_cursor = protocol_cursor.clone()
        block = [FCell("Σ+", LIGHT_GRAY_BG, align="RIGHT", width=40)]
        for i in range(len(participants)):
            block.extend([FCell(SumPlus(theme_metadata, i), merge_shift_y=1), None])
        protocol_cursor.add_block(block)
        for val in (50, 40, 30, 20, 10):
            protocol_cursor.add_block(
                [FCell(val, LIGHT_GRAY_BG, width=25)]
                + [
                    FCell(CountValAnswers(theme_metadata, val), merge_shift_y=1),
                    None,
                ]
                * len(participants)
            )
        cursor_for_sums.clone().add_block(
            [FCell("Σ", LIGHT_GRAY_BG, align="CENTER", width=40)]
            + [
                FCell(
                    SumTotal(theme_metadata),
                    BOTTOM_BORDER,
                    merge_shift_y=1,
                ),
                None,
            ]
            * len(participants),
        )
        for i in range(len(participants)):
            y_shift = 1 + i * 2
            self.team_stats.add_block(
                [
                    f"Бой {match_id}",
                    "=" + cursor_for_sums.clone_shift(x=-1, y=y_shift).as_cell_full(),
                    "=" + cursor_for_sums.clone_shift(y=y_shift).as_cell_full(),
                    "=" + cursor_for_sums.clone_shift(x=1, y=y_shift).as_cell_full(),
                    "=" + sum_plus_cursor.clone_shift(y=y_shift).as_cell_full(),
                    "=" + sum_plus_cursor.clone_shift(x=1, y=y_shift).as_cell_full(),
                    "=" + sum_plus_cursor.clone_shift(x=2, y=y_shift).as_cell_full(),
                    "=" + sum_plus_cursor.clone_shift(x=3, y=y_shift).as_cell_full(),
                    "=" + sum_plus_cursor.clone_shift(x=4, y=y_shift).as_cell_full(),
                ],
                horizontal=True,
                add_newline=True,
            )
        cursor_for_places.clone().add_block(
            [FCell("М", LIGHT_GRAY_BG, width=32, align="CENTER")]
            + [
                FCell(None, BOTTOM_BORDER, merge_shift_y=1),
                None,
            ]
            * len(participants)
        )
        protocol_cursor.set(init_cursor.clone_shift(y=2 + 2 * len(participants)))
        table_init_cursor = table_cursor.clone()
        table_cursor.add_block(
            [FCell(f"Бой {match_id}", LIGHT_GRAY_BG)]
            + [
                "="
                + make_address(
                    protocol_cursor.wks, init_cursor.clone_shift(y=1 + i * 2)
                )
                for i in range(len(participants))
            ]
        )
        sums_cursor = table_cursor.clone()
        table_cursor.add_block(
            [FCell("Σ", LIGHT_GRAY_BG, width=40, align="CENTER")]
            + [
                "="
                + make_address(
                    protocol_cursor.wks, init_cursor.clone_shift(y=1 + i * 2, x=1)
                )
                for i in range(len(participants))
            ]
        )
        places_cursor = table_cursor.clone()
        table_cursor.add_block(
            [FCell("М", LIGHT_GRAY_BG, width=32, align="CENTER")]
            + [
                "="
                + make_address(
                    protocol_cursor.wks, init_cursor.clone_shift(y=1 + i * 2, x=2)
                )
                for i in range(len(participants))
            ]
        )
        if add_newline:
            table_cursor.set(table_init_cursor.clone_shift(y=6))
        else:
            table_cursor.set(table_init_cursor.clone_shift(x=4))
        self.match_counter += 1
        return MatchCursors(
            sums_cursor, places_cursor, cursor_for_places, sum_plus_cursor, match_id
        )

    def init_participant(self, basket, participant):
        return f"""=IF({self.zh_finished.as_cell_full()}, {self.venues_to_cursors[basket].clone_shift(y=participant).as_cell_full()}, "Корзина {basket}-{participant + 1}")"""
