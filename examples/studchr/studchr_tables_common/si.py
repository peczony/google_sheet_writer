from typing import NamedTuple

from google_sheet_writer import (
    Cf,
    Checkbox,
    Cursor,
    FCell,
    GoogleSheetWriter,
    col_to_a,
    make_address,
)
from gspread.utils import (
    rowcol_to_a1,
)
from gspread_formatting import (
    BooleanCondition,
    BooleanRule,
    CellFormat,
    ColorStyle,
    ConditionalFormatRule,
    GridRange,
    TextFormat,
    border,
    borders,
)

from studchr_tables_common.utils import (
    GREEN,
    GREEN_BG,
    LIGHT_GRAY,
    LIGHT_GRAY_BG,
    RED,
    RED_BG,
    WHITE,
)

ALL_BORDERS = CellFormat(
    borders=borders(
        bottom=border("SOLID", LIGHT_GRAY),
        top=border("SOLID", LIGHT_GRAY),
        left=border("SOLID", LIGHT_GRAY),
        right=border("SOLID", LIGHT_GRAY),
    )
)


def init_worksheet(wks):
    def _init_worksheet(wks, max_row=0, max_col=0):
        range_all = f"A1:{col_to_a(max_col)}:{max_row}"
        wks.hide_gridlines()
        wks.set_cell_format(
            range_all,
            Cf.font_family("Open Sans") + Cf.font_size(11) + Cf.background_color(WHITE),
        )
        for i in range(1, max_row):
            rh_dict = wks.parent.fmt[wks.ws.title]["row_heights"]
            if i not in rh_dict:
                rh_dict[i] = 23
        wks.add_to_batch_clear(range_all)

    wks.init_format = _init_worksheet


def get_red_green_rules(worksheet, range_start, range_end):
    range_ = (
        rowcol_to_a1(range_start.y, range_start.x)
        + ":"
        + rowcol_to_a1(range_end.y, range_end.x)
    )
    result = []
    for symbol in ["w", "ц", "W", "Ц"]:
        result.append(
            ConditionalFormatRule(
                ranges=[GridRange.from_a1_range(range_, worksheet.ws)],
                booleanRule=BooleanRule(
                    condition=BooleanCondition("TEXT_EQ", [symbol]),
                    format=CellFormat(
                        textFormat=TextFormat(
                            foregroundColorStyle=ColorStyle(rgbColor=RED)
                        ),
                        backgroundColor=RED,
                    ),
                ),
            )
        )
    for symbol in ["q", "й", "Q", "Й"]:
        result.append(
            ConditionalFormatRule(
                ranges=[GridRange.from_a1_range(range_, worksheet.ws)],
                booleanRule=BooleanRule(
                    condition=BooleanCondition("TEXT_EQ", [symbol]),
                    format=CellFormat(
                        textFormat=TextFormat(
                            foregroundColorStyle=ColorStyle(rgbColor=GREEN)
                        ),
                        backgroundColor=GREEN,
                    ),
                ),
            ),
        )
    # the following rule is needed because if there is no white background,
    # adjacent green/red cells bleed unpleasantly into non-colored white cells
    result.append(
        ConditionalFormatRule(
            ranges=[GridRange.from_a1_range(range_, worksheet.ws)],
            booleanRule=BooleanRule(
                condition=BooleanCondition("TEXT_EQ", [""]),
                format=CellFormat(
                    backgroundColor=WHITE,
                ),
            ),
        ),
    )
    return result


def is_right(cell_a1):
    return f'OR(EXACT("q", TRIM(LOWER({cell_a1}))), EXACT("й", TRIM(LOWER({cell_a1}))))'


def is_wrong(cell_a1):
    return f'OR(EXACT("w", TRIM(LOWER({cell_a1}))), EXACT("ц", TRIM(LOWER({cell_a1}))))'


class MatchCursors(NamedTuple):
    sums_cursor: Cursor
    places_cursor: Cursor
    places_protocol_cursor: Cursor
    sum_plus_cursor: Cursor
    match_id: str | int


class ThemeSumFunc:
    def __init__(self, participant_num):
        self.p = participant_num

    def __call__(self, cur):
        vals = []
        for i in range(1, 6):
            cell = cur.clone_shift(x=-6 + i)
            val_cell = cell.clone_shift(y=-(1 + self.p))
            vals.append(
                f"IF({is_right(cell)}, {val_cell:pr}, IF({is_wrong(cell)}, -{val_cell:pr}, 0))"
            )
        return "=" + " + ".join(vals)


class SumPlus:
    def __init__(self, theme_metadata, participant_num):
        self.theme_metadata = theme_metadata
        self.p = participant_num

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            for n in (10, 20, 30, 40, 50):
                xcur = dct[n]
                cell = cur.replace(x=xcur.x)
                vals.append(
                    f"IF({is_right(cell.as_cell())}, {cell.clone_shift(y=-(1 + self.p)).as_cell()}, 0)"
                )
        return "=" + " + ".join(vals)


class SumTotal:
    def __init__(self, theme_metadata):
        self.theme_metadata = theme_metadata

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            sum_col = dct["sum"]
            cell = cur.replace(x=sum_col.x)
            vals.append(cell.as_cell())
        return "=" + " + ".join(vals)


class CountValAnswers:
    def __init__(self, theme_metadata, val):
        self.theme_metadata = theme_metadata
        self.val = val

    def __call__(self, cur):
        vals = []
        for theme in self.theme_metadata:
            dct = self.theme_metadata[theme]
            xcur = dct[self.val]
            cell = cur.replace(x=xcur.x)
            vals.append(f"IF({is_right(cell.as_cell())}, 1, 0)")
        return "=" + " + ".join(vals)


def add_formats(f1, f2):
    if f1 is None and f2 is None:
        return None
    if f1 is None and f2 is not None:
        return f2
    if f1 is not None and f2 is None:
        return f1
    if f1 is not None and f2 is not None:
        return f1 + f2


def magic_header(heading, hidden=False):
    return [
        FCell(heading, Cf.ITALIC, hidden=hidden),
        FCell("Place", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Points", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Plus", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(50, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(40, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(30, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(20, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(10, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Toss", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
    ]


def get_place(match_cursors, place):
    pc = match_cursors.places_protocol_cursor.clone_shift(y=1)
    return f'=IFERROR(VLOOKUP({place}, {pc.as_cell_full()}:{pc.clone_shift(x=1, y=3).as_cell_full()}, 2, FALSE), "{match_cursors.match_id}-{place}")'


class SiMaker(GoogleSheetWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.match_counter = 1

    def add_si_theme(self, protocol_cursor, theme_num, n_participants=3):
        metadata = {}
        range_start = protocol_cursor.clone_shift(y=1)
        for i in range(1, 6):
            theme_val = i * 10
            metadata[theme_val] = protocol_cursor.clone()
            protocol_cursor.set_col_width(25)
            protocol_cursor.clone_shift(y=1).vblock(
                [FCell(None, ALL_BORDERS)] * n_participants
            )
            protocol_cursor.add_block([FCell(theme_val, LIGHT_GRAY_BG)])
        range_end = protocol_cursor.clone_shift(x=-1, y=n_participants)
        protocol_cursor.wks.add_to_conditional_formatting(
            get_red_green_rules(protocol_cursor.wks, range_start, range_end)
        )
        metadata["sum"] = protocol_cursor.clone()
        protocol_cursor.set_col_width(50 + 5 * len(str(theme_num)))
        protocol_cursor.align_column("RIGHT")
        protocol_cursor.add_block(
            [FCell(f"Тема {theme_num}", LIGHT_GRAY_BG)]
            + [ThemeSumFunc(i) for i in range(n_participants)]
        )
        protocol_cursor.add_block([FCell(None, LIGHT_GRAY_BG, width=30)])
        return metadata

    def get_plosch_formula(self, i):
        mesto = self.plosch.spawn_cursor(x=2, y=1 + i).as_cell_full()
        ved = self.plosch.spawn_cursor(x=3, y=1 + i).as_cell_full()
        assist = self.plosch.spawn_cursor(x=4, y=1 + i).as_cell_full()
        return (
            f"""=CONCATENATE("Пл. {i} (", {mesto}, ", ", {ved}, "/", {assist}, ")")"""
        )

    def add_si_match(
        self,
        table_cursor,
        protocol_cursor,
        participants,
        themes=8,
        match_id=None,
        add_newline=False,
        italic_headline=False,
        match_id_suffix="",
        table_bg_color=None,
        add_tiebreak=False,
        shift_themes=0,
    ) -> MatchCursors:
        if match_id is None:
            match_id = col_to_a(self.match_counter)
            self.match_counter += 1
        init_cursor = protocol_cursor.clone()
        protocol_cursor.add_block(
            [FCell(f"Бой {match_id}", LIGHT_GRAY_BG, width=180)] + participants
        )
        protocol_cursor.set_col_width(50)
        cursor_for_sums = protocol_cursor.clone()
        protocol_cursor.x += 1
        protocol_cursor.set_col_width(50)
        cursor_for_places = protocol_cursor.clone()
        protocol_cursor.freeze_col()
        protocol_cursor.x += 1
        protocol_cursor.hide_col()
        protocol_cursor.add_block(
            ["Игрок_hidden"]
            + [lambda x: "=" + x.clone_shift(x=-3).as_cell()] * len(participants)
        )
        theme_metadata = {}
        for i in range(themes):
            theme_num = i + 1
            theme_metadata[theme_num] = self.add_si_theme(
                protocol_cursor, theme_num, n_participants=len(participants)
            )

        if shift_themes:
            protocol_cursor.shift(x=7 * shift_themes)
        protocol_cursor.set_col_width(40)
        if add_tiebreak:
            protocol_cursor.vblock(
                [FCell("П", Cf.RIGHT + LIGHT_GRAY_BG, width=40)]
                + [0] * len(participants)
            )
        sum_plus_cursor = protocol_cursor.clone()
        protocol_cursor.add_block(
            [FCell("Σ+", Cf.RIGHT + LIGHT_GRAY_BG, width=40)]
            + [SumPlus(theme_metadata, i) for i in range(len(participants))]
        )
        for val in (50, 40, 30, 20, 10):
            protocol_cursor.set_col_width(25)
            protocol_cursor.add_block(
                [FCell(val, LIGHT_GRAY_BG)]
                + [CountValAnswers(theme_metadata, val)] * len(participants)
            )
        cursor_for_sums.clone().add_block(
            [FCell("Σ", Cf.RIGHT + LIGHT_GRAY_BG, width=40)]
            + [SumTotal(theme_metadata)] * len(participants),
        )
        cursor_for_places.clone().add_block(
            [FCell("М", Cf.RIGHT + LIGHT_GRAY_BG, width=32)]
        )
        protocol_cursor.set(init_cursor.clone_shift(y=2 + len(participants)))
        table_init_cursor = table_cursor.clone()
        if italic_headline:
            hfmt = Cf.ITALIC
        else:
            hfmt = None
        if table_bg_color:
            hfmt = add_formats(hfmt, table_bg_color)
        table_cursor.add_block(
            [FCell(f"Бой {match_id}" + match_id_suffix, hfmt, width=150)]
            + [
                "="
                + make_address(protocol_cursor.wks, init_cursor.clone_shift(y=1 + i))
                for i in range(len(participants))
            ]
        )
        sums_cursor = table_cursor.clone()
        if italic_headline:
            hfmt2 = Cf.ITALIC + Cf.RIGHT
        else:
            hfmt2 = Cf.RIGHT
        if table_bg_color:
            hfmt2 = add_formats(hfmt2, table_bg_color)
        table_cursor.add_block(
            [FCell("Σ", hfmt2, width=40)]
            + [
                "="
                + make_address(
                    protocol_cursor.wks, init_cursor.clone_shift(y=1 + i, x=1)
                )
                for i in range(len(participants))
            ]
        )
        places_cursor = table_cursor.clone()
        table_cursor.add_block(
            [FCell("М", hfmt2, width=28, align="RIGHT")]
            + [
                "="
                + make_address(
                    protocol_cursor.wks, init_cursor.clone_shift(y=1 + i, x=2)
                )
                for i in range(len(participants))
            ]
        )
        if add_newline:
            table_cursor.set(table_init_cursor.clone_shift(y=6))
        else:
            table_cursor.set(table_init_cursor.clone_shift(x=4))
        if hasattr(self, "stats"):
            for i, p in enumerate(participants):
                self.stats.cursor.hblock_n(
                    [match_id, p, "=" + sums_cursor.clone_shift(y=i + 1).as_cell_full()]
                )
        return MatchCursors(
            sums_cursor, places_cursor, cursor_for_places, sum_plus_cursor, match_id
        )

    def add_po_next(
        self,
        stage_num=None,
        hr_prev_stage_num=None,
        po_composition=None,
        n_green=None,
        skip_matches_for_stats=0,
        themes=8,
        shift_themes=0,
        add_magic=True,
    ) -> list[MatchCursors]:
        po = self.po
        po_prot = self.po_prot
        cur = self.cur
        sort_cur = self.sort_cur
        po.cursor.x += 4
        po.cursor.y = 1
        po.add_block(
            [FCell(f"{stage_num} этап", Cf.font_size(13))],
            horizontal=True,
            add_newline=True,
        )
        po.cursor.shift(y=1)
        bc = po_prot.cursor.add_block(
            [
                FCell(
                    f"{hr_prev_stage_num} этап ПО окончен",
                    Cf.WRAP + Cf.RIGHT + Cf.MIDDLE,
                    height=40,
                    merge_shift_x=1,
                ),
                None,
                FCell(Checkbox(), Cf.CENTER),
            ],
            horizontal=True,
            add_newline=True,
        )
        po_prot.cursor.y += 1
        previous_stage_finished = po_prot.spawn_cursor(x=bc.max_x, y=bc.max_y)
        po_prot.cursor.add_block(
            [FCell(f"{stage_num} этап", Cf.font_size(13), height=40)],
            add_newline=True,
        )
        if add_magic:
            cur.x = 1
            cur.add_block(
                magic_header(f"PO-{stage_num}", hidden=True),
                add_newline=True,
                horizontal=True,
            )
            init_cur = cur.clone()
        total_participants = 0
        all_match_cursors = []
        for match_num, match in enumerate(po_composition):
            po.cursor.clone_shift(y=1, x=-1).add_block(match)
            match_cursors = self.add_si_match(
                po.cursor,
                po_prot.cursor,
                [
                    f'=IF({previous_stage_finished.as_cell_full()}, {self.after_prev.clone_shift(y=n - 1).as_cell_full()}, "ПО{stage_num - 1}-{n}")'
                    for n in match
                ],
                add_newline=True,
                table_bg_color=GREEN_BG if match_num < n_green else RED_BG,
                add_tiebreak=True,
                themes=themes,
                shift_themes=shift_themes,
            )
            all_match_cursors.append(match_cursors)
            places_cursor = match_cursors.places_cursor
            sum_plus_cursor = match_cursors.sum_plus_cursor
            if not match_num < skip_matches_for_stats:
                total_participants += len(match)
                for i in range(len(match)):
                    p_num = i + 1
                    cur.add_block(
                        [
                            "="
                            + places_cursor.clone_shift(y=p_num, x=-2).as_cell_full(),
                            "="
                            + places_cursor.clone_shift(y=p_num).as_cell_full()
                            + f" + {10 if match_num < n_green else 20}",
                            "="
                            + places_cursor.clone_shift(y=p_num, x=-1).as_cell_full(),
                        ]
                        + [
                            "="
                            + sum_plus_cursor.clone_shift(y=p_num, x=i).as_cell_full()
                            for i in range(6)
                        ],
                        add_newline=True,
                        horizontal=True,
                    )
        if add_magic:
            sort_cur.add_block(
                magic_header(f"PO-{stage_num}"), add_newline=True, horizontal=True
            )
            self.after_prev = sort_cur.clone()
            sort_cur.add_block(
                [
                    f"=SORT(A{init_cur.y}:J{init_cur.y + total_participants - 1}, 2, 1, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"
                ],
                add_newline=True,
            )
            sort_cur.y += total_participants - 1
        return all_match_cursors

    def add_po_wo_shuffle(
        self,
        matches=None,
        n_green=None,
        stage_name=None,
        themes=8,
        override_po=None,
        override_po_prot=None,
    ) -> list[MatchCursors]:
        po = override_po or self.po
        po_prot = override_po_prot or self.po_prot
        po.cursor.x += 4
        po.cursor.y = 1
        po.cursor.add_block([FCell(stage_name, Cf.font_size(13))], add_newline=True)
        po.cursor.y = 3
        po_prot.cursor.add_block(
            [FCell(stage_name, Cf.font_size(13), height=40)], add_newline=True
        )
        total_participants = 0
        all_match_cursors = []
        for match_num, match in enumerate(matches):
            total_participants += len(match)
            match_cursors = self.add_si_match(
                po.cursor,
                po_prot.cursor,
                match,
                add_newline=True,
                table_bg_color=GREEN_BG if match_num < n_green else RED_BG,
                add_tiebreak=True,
                themes=themes,
            )
            all_match_cursors.append(match_cursors)
        return all_match_cursors
