#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from gspread_formatting import (
    ConditionalFormatRule,
    GridRange,
    BooleanRule,
    BooleanCondition,
    CellFormat,
    TextFormat,
    ColorStyle,
)

from google_sheet_writer import GoogleSheetWriter, FCell, Cf, Checkbox, col_to_a
from studchr_tables_common import init_worksheet, GREEN


def get_red_green_rules(cursor1, cursor2):
    a1_range = f"{cursor1.as_cell()}:{cursor2.as_cell()}"
    return [
        ConditionalFormatRule(
            ranges=[GridRange.from_a1_range(a1_range, cursor1.wks.ws)],
            booleanRule=BooleanRule(
                condition=BooleanCondition("NUMBER_EQ", ["1"]),
                format=CellFormat(
                    textFormat=TextFormat(
                        foregroundColorStyle=ColorStyle(rgbColor=GREEN)
                    ),
                    backgroundColor=GREEN,
                ),
            ),
        )
    ]


class OdMaker(GoogleSheetWriter):
    def __init__(self, *args, **kwargs):
        self.n_teams = kwargs.pop("n_teams")
        self.tour_comp = self.parse_tour_comp(kwargs.pop("tour_comp"))
        super().__init__(*args, **kwargs)

    @classmethod
    def parse_tour_comp(cls, tour_comp):
        result = []
        segments = tour_comp.split(",")
        for seg in segments:
            if "*" in seg:
                before, after = seg.split("*", 1)
                result.extend([int(before.strip())] * int(after.strip()))
            else:
                result.append(int(seg.strip()))
        return result

    def get_plus_formula(self, q_num):
        col = self.q_to_col[q_num]
        col_a = col_to_a(col)
        cell_start = self.entry.spawn_cursor(y=3, x=col).as_cell_full()
        cell_checkbox = self.entry.spawn_cursor(y=2, x=col).as_cell_full()
        return (
            lambda c: f"""=IF(OR(ISNA(MATCH({c.replace(x=1).as_cell()},{cell_start}:{col_a},0)), NOT({cell_checkbox})), "", 1)"""
        )

    @classmethod
    def get_tour_sum_formula(cls, tour):
        def formula(cell):
            return f"=SUM({cell.clone_shift(x=-tour).as_cell()}:{cell.clone_shift(x=-1).as_cell()})"

        return formula

    @classmethod
    def get_tour_rating_formula(cls, tour):
        def formula(cell):
            return f"=SUMPRODUCT({cell.clone_shift(x=-tour-1)}:{cell.clone_shift(x=-2)}, {cell.clone_shift(x=-tour-1).set(y=3)}:{cell.clone_shift(x=-2).set(y=3)})"

        return formula

    def get_sum_before_last_formula(self, end_col):
        def _sum_before_last(x):
            points_range = f"{x.clone_shift(x=2)}:{x.replace(x=end_col)}"
            checkboxes_range = (
                f"{x.clone_shift(x=2).set(y=2)}:{x.replace(x=end_col, y=2)}"
            )
            question_numbers_range = (
                f"{x.clone_shift(x=2).set(y=1)}:{x.replace(x=end_col, y=1)}"
            )
            return f"""=SUM(FILTER({points_range}*{checkboxes_range}, {question_numbers_range}<{self.last_question:f}))"""

        return _sum_before_last

    def get_place_formula(self, rank_col):
        def _get_place_formula(x):
            place_cur = x.replace(x=rank_col)
            places_start = x.replace(x=rank_col, y=2)
            places_end = x.replace(x=rank_col, y=2 + self.n_teams)
            count_if = f"COUNTIF({places_start}:{places_end},{place_cur})"
            return f"""=IF({count_if}=1,{place_cur},{place_cur}&"–"&{place_cur}+{count_if}-1)"""

        return _get_place_formula

    def get_up_down_formula(self, rank_col):
        def _get_up_down_formula(x):
            rank = x.replace(x=rank_col)
            rank_n_1 = x.replace(x=rank_col + 1)
            return f"""=IF({rank}<{rank_n_1}, "↑", IF({rank}>{rank_n_1}, "↓", ""))"""

        return _get_up_down_formula

    def make_od(self):
        self.itog = self.get_worksheet("Итог", remove=True, defer=True)
        self.podr = self.get_worksheet("Подробно", remove=True, defer=True)
        init_worksheet(self.itog)
        init_worksheet(self.podr)
        podr = self.podr
        itog = self.itog
        self.entry = self.get_worksheet("Ввод", remove=True)
        self.tourn_site = self.get_worksheet("Для турнирного сайта", remove=True)
        init_worksheet(self.entry)
        init_worksheet(self.tourn_site)
        entry = self.entry
        tourn_site = self.tourn_site
        q_counter = 1
        entry.cursor.set(x=2, y=1)
        self.q_to_col = {}
        entry.spawn_cursor(x=1, y=2).freeze_row()
        entry.force_max_row = 53
        tourn_site_row = [
            FCell("Team ID", width=40),
            FCell("Название", width=200),
        ]
        for tour in self.tour_comp:
            for _ in range(tour):
                self.q_to_col[q_counter] = entry.cursor.x
                entry.cursor.add_block(
                    [
                        FCell(q_counter, align="CENTER", width=30),
                        Checkbox(),
                    ]
                )
                tourn_site_row.append(FCell(q_counter, align="CENTER", width=30))
                q_counter += 1
            entry.cursor.add_block([FCell(None, align="CENTER", width=30)])
        tourn_site.add_block(tourn_site_row, horizontal=True, add_newline=True)
        end_col = entry.cursor.x - 1
        entry.cursor.set(x=1, y=1)
        blk = entry.cursor.add_block(
            [
                FCell("Введён вопрос:", width=122, align="CENTER"),
                lambda x: f"""=iferror(max(filter({x.clone_shift(x=1, y=-1)}:{x.clone_shift(y=-1).replace(x=end_col)},{x.clone_shift(x=1)}:{x.replace(x=end_col)}=true)), "")""",
            ]
        )
        self.last_question = entry.spawn_cursor(x=blk.max_x, y=blk.max_y)
        blk = podr.cursor.add_block(
            [
                FCell("№", Cf.ITALIC, width=30, align="CENTER"),
                FCell("ID", Cf.ITALIC + Cf.RIGHT, width=40, hidden=True),
                FCell("Команда", Cf.ITALIC, width=200),
                FCell("Σ", Cf.ITALIC, width=50, align="CENTER"),
                FCell("Σ(n-1)", Cf.ITALIC, width=50, align="CENTER", hidden=True),
                FCell("R", Cf.ITALIC, width=50, align="CENTER"),
            ],
            horizontal=True,
            add_newline=True,
        )
        podr.cursor.shift(y=3)
        teams_start = podr.cursor.clone()
        podr.cursor.add_block(list(range(1, self.n_teams + 1)))
        tourn_site.add_block(
            [
                f"={teams_start.clone_shift(x=1, y=i).as_cell_full()}"
                for i in range(self.n_teams)
            ]
        )
        tourn_site.add_block(
            [
                f"={teams_start.clone_shift(x=2, y=i).as_cell_full()}"
                for i in range(self.n_teams)
            ]
        )
        podr.cursor.set(y=1, x=blk.max_x + 1)
        q_counter = 1
        tour_cols = []
        for i, tour in enumerate(self.tour_comp):
            tour_num = i + 1
            cl = podr.cursor.clone_shift(y=4)
            podr.add_to_conditional_formatting(
                get_red_green_rules(cl, cl.clone_shift(x=tour - 1, y=self.n_teams - 1))
            )
            for _ in range(tour):
                q_start = podr.cursor.clone_shift(y=4)
                podr.cursor.add_block(
                    [
                        FCell(q_counter, Cf.ITALIC, align="CENTER", width=26),
                        FCell(
                            lambda x: f"=IF({entry.spawn_cursor(x=self.q_to_col[q_counter], y=2):f}, 1, 0)",
                            align="CENTER",
                            hidden_row=True,
                        ),
                        FCell(
                            lambda x: f"=COUNT({teams_start.as_cell()}:{teams_start.clone_shift(y=self.n_teams).as_cell()})-{x.clone_shift(y=1).as_cell()}",
                            hidden_row=True,
                        ),
                        FCell(
                            lambda x: f"=COUNT({x.clone_shift(y=1).as_cell()}:{x.clone_shift(y=self.n_teams + 1).as_cell()})",
                            hidden_row=True,
                        ),
                    ]
                    + [self.get_plus_formula(q_counter)] * self.n_teams
                )
                tourn_site.add_block(
                    [
                        f"={q_start.clone_shift(y=i).as_cell_full()}"
                        for i in range(self.n_teams)
                    ]
                )
                q_counter += 1
            tour_cols.append(podr.cursor.x)
            podr.cursor.add_block(
                [
                    FCell(f"Т{tour_num}", Cf.ITALIC, align="CENTER", width=40),
                    None,
                    None,
                    None,
                ]
                + [self.get_tour_sum_formula(tour)] * self.n_teams
            )
            podr.cursor.add_block(
                [
                    FCell(
                        f"R{tour_num}", Cf.ITALIC, align="CENTER", width=40, hidden=True
                    ),
                    None,
                    None,
                    None,
                ]
                + [self.get_tour_rating_formula(tour)] * self.n_teams
            )
        end_col = podr.cursor.x - 1
        podr.cursor.set(x=4, y=teams_start.y)
        podr.spawn_cursor(x=1, y=1).freeze_row()
        sum_start = podr.cursor.clone()
        podr.cursor.add_block(
            [lambda x: "=" + "+".join([x.replace(x=n).as_cell() for n in tour_cols])]
            * self.n_teams
        )
        podr.cursor.add_block(
            [self.get_sum_before_last_formula(end_col)] * self.n_teams
        )
        podr.cursor.add_block(
            [
                lambda x: "="
                + "+".join([x.replace(x=n + 1).as_cell() for n in tour_cols])
            ]
            * self.n_teams
        )
        itog_header = (
            [
                FCell(None, width=30, align="CENTER"),
                FCell("Место", Cf.ITALIC, width=60, align="CENTER"),
                FCell("Команда", Cf.ITALIC, width=240),
                FCell("Σ", Cf.ITALIC, width=50, align="CENTER"),
                FCell("R", Cf.ITALIC, width=50, align="CENTER"),
            ]
            + [
                FCell(f"T{i + 1}", Cf.ITALIC, width=50, align="CENTER")
                for i in range(len(self.tour_comp))
            ]
            + [
                FCell("Σ(n-1)", Cf.ITALIC, width=50, align="CENTER", hidden=True),
                FCell("Rank", Cf.ITALIC, width=70, align="CENTER", hidden=True),
                FCell("Rank(n-1)", Cf.ITALIC, width=70, align="CENTER", hidden=True),
                FCell("Sort(Σ+R)", Cf.ITALIC, width=70, align="CENTER", hidden=True),
            ]
        )
        blk = itog.add_block(
            itog_header,
            horizontal=True,
            add_newline=True,
        )
        itog.cursor.shift(x=1)
        places_cursor = itog.cursor.clone()
        itog.cursor.shift(x=1)
        itog.add_block(
            [
                FCell(f"={teams_start.clone_shift(y=i, x=2):f}", hidden_row=True)
                for i in range(self.n_teams)
            ]
        )
        itog.add_block(
            [f"={sum_start.clone_shift(y=i):f}" for i in range(self.n_teams)]
        )
        itog.add_block(
            [f"={sum_start.clone_shift(y=i, x=2):f}" for i in range(self.n_teams)]
        )  # rating
        for tc in tour_cols:
            itog.add_block(
                [
                    f"={podr.spawn_cursor(y=teams_start.y + i, x=tc):f}"
                    for i in range(self.n_teams)
                ]
            )
        itog.add_block(
            [f"={sum_start.clone_shift(y=i, x=1):f}" for i in range(self.n_teams)]
        )  # sum n-1
        rank_col = itog.cursor.x
        itog.add_block(
            [
                lambda x: f"=RANK({x.replace(x=4)}, {x.replace(x=4, y=2)}:{x.replace(x=4, y=1+self.n_teams)})"
                for _ in range(self.n_teams)
            ]
        )  # rank
        itog.add_block(
            [
                lambda x: f"=RANK({x.clone_shift(x=-2)}, {x.clone_shift(x=-2).set(y=2)}:{x.clone_shift(x=-2).set(y=1+self.n_teams)})"
                for _ in range(self.n_teams)
            ]
        )  # rank(n-1)
        itog.add_block(
            [
                lambda x: f"={x.replace(x=4)}*10000+{x.replace(x=5)}"
                for _ in range(self.n_teams)
            ]
        )  # sort
        places_cursor.add_block(
            [self.get_place_formula(rank_col) for _ in range(self.n_teams)]
        )
        itog.cursor.set(x=1, y=2 + self.n_teams)
        up_down = itog.cursor.clone_shift(y=1)
        start_cur = itog.spawn_cursor(x=2, y=2)
        end_cur = itog.spawn_cursor(x=rank_col + 2, y=1 + self.n_teams)
        itog.cursor.clone().add_block([FCell(None, hidden_row=True)])
        itog.cursor.add_block(
            itog_header,
            horizontal=True,
            add_newline=True,
        )
        curr_q = itog.spawn_cursor(y=1, x=blk.max_x + 1)
        fr = itog.spawn_cursor(y=1, x=5)
        fr.freeze_row()
        fr.freeze_col()
        curr_q.add_block(
            [
                FCell(
                    f"""=IF({self.last_question:f}="", "Турнир не начался", "Введён вопрос "&{self.last_question:f})""",
                    Cf.ITALIC + Cf.CENTER,
                    width=150,
                )
            ]
        )

        itog.cursor.shift(x=1)
        itog.cursor.add_block(
            [
                f"=SORT({start_cur}:{end_cur}, {rank_col - start_cur.x + 3}, FALSE, 2, TRUE)"
            ]
        )
        up_down.add_block([self.get_up_down_formula(rank_col)] * self.n_teams)
        self.submit_order = [entry.ws.title, podr.ws.title, itog.ws.title]
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--tour_comp", "-tc", default="15*6")
    parser.add_argument("--n_teams", "-n", default=50, type=int)
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)
    maker = OdMaker(
        gc, args.spreadsheet_id, n_teams=args.n_teams, tour_comp=args.tour_comp
    )
    maker.make_od()


if __name__ == "__main__":
    main()
