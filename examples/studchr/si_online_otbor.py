#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import random
from typing import NamedTuple
import enum

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    Cursor,
    FCell,
    col_to_a,
)
from studchr_tables_common import (
    GREEN_BG,
    LIGHT_GRAY_BG,
    RED_BG,
    init_worksheet,
    SiMaker,
)


class PlayoffEnum(enum.Enum):
    PO24_2 = "po24_2"
    PO16 = "po16"


def enumtype(astring):
    try:
        return PlayoffEnum[astring.upper()]
    except KeyError:
        msg = ", ".join([t.name.lower() for t in PlayoffEnum])
        msg = f"PlayoffEnum: use one of {msg}"
        raise argparse.ArgumentTypeError(msg)


PO_2 = [
    [1, 8, 9, 16],
    [2, 7, 10, 15],
    [3, 6, 11, 14],
    [4, 5, 12, 13],
    [17, 24, 25, 32],
    [18, 23, 26, 31],
    [19, 22, 27, 30],
    [20, 21, 28, 29],
]
PO_3 = [
    [1, 4, 5, 8],
    [2, 3, 6, 7],
    [9, 16, 17, 24],
    [10, 15, 18, 23],
    [11, 14, 19, 22],
    [12, 13, 20, 21],
]
PO_4 = [
    [1, 2, 3, 4],
    [5, 10, 11, 16],
    [6, 9, 12, 15],
    [7, 8, 13, 14],
]
PO_5 = [
    [3, 6, 7, 10],
    [4, 5, 8, 9],
]


class GroupCursors(NamedTuple):
    table_cursor: Cursor
    match_cursor: Cursor


def calc_group_points(cur):
    return f'=IF({cur.clone_shift(x=-1).as_cell()}="","",4-{cur.clone_shift(x=-1).as_cell()}+{cur.clone_shift(x=-2).as_cell()}/1000)'


def filter_protocols_by_column(cell, protocols, cursor):
    col = col_to_a(cursor.x)
    sums = []
    for pr in protocols:
        title = protocols[pr].ws.title
        sum_ = (
            f"SUM(IFNA(FILTER('{title}'!{col}1:{col}99, '{title}'!A1:A99 = {cell}), 0))"
        )
        sums.append(sum_)
    return "=" + " + ".join(sums)


def magic_header(heading, hidden=False):
    return [
        FCell(heading, Cf.ITALIC, hidden=hidden),
        FCell("Place", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Points", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Plus", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(50, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(40, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(30, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(20, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell(10, Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
        FCell("Toss", Cf.ITALIC, align="RIGHT", width=50, hidden=hidden),
    ]


def group_header(hidden=False):
    return [
        FCell("Игрок", Cf.ITALIC, width=150, hidden=hidden),
        FCell("Очки", Cf.ITALIC + Cf.RIGHT, width=42, hidden=hidden),
        FCell("Круг 1", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 2", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 3", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 4", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
    ]


def get_place(match_cursors, place):
    pc = match_cursors.places_protocol_cursor.clone_shift(y=1)
    return f'=IFERROR(VLOOKUP({place}, {pc.as_cell_full()}:{pc.clone_shift(x=1,y=3).as_cell_full()}, 2, FALSE), "{match_cursors.match_id}-{place}")'


class GroupRange(NamedTuple):
    start: Cursor
    end: Cursor


class OnlineOtborMaker(SiMaker):
    def add_po_1(self):
        po = self.po
        po_prot = self.po_prot
        cur = self.cur
        po.cursor.add_block([FCell(None, width=34)])
        po.cursor.set(x=2, y=1)
        po.add_block(
            [FCell("1 этап", Cf.font_size(13))], horizontal=True, add_newline=True
        )
        po.cursor.shift(y=1)
        po_prot.cursor.add_block(
            [FCell("1 этап", Cf.font_size(13), height=40)], add_newline=True
        )
        cur.y = 26
        cur.x = 1
        cur.add_block(
            magic_header("PO-1", hidden=True),
            add_newline=True,
            horizontal=True,
        )
        n_green = 8
        total_participants = 0
        for i in range(8):
            # match = self.
            match = [
                "="
                + self.init_qual_cells[i + 1]
                .start.clone_shift(y=x, x=-2)
                .as_cell_full()
                for x in range(self.players_in_group_match)
            ]
            po.cursor.clone_shift(y=1, x=-1).add_block(match)
            total_participants += len(match)
            match_cursors = self.add_si_match(
                po.cursor,
                po_prot.cursor,
                match,
                add_newline=True,
                table_bg_color=GREEN_BG if i < n_green else RED_BG,
                match_id_suffix=f" (пл. {int(i + 1)})",
            )
            places_cursor = match_cursors.places_cursor
            sum_plus_cursor = match_cursors.sum_plus_cursor
            for j in range(len(match)):
                p_num = j + 1
                cur.add_block(
                    [
                        "=" + places_cursor.clone_shift(y=p_num, x=-2).as_cell_full(),
                        f"={places_cursor.clone_shift(y=p_num):f} + {10 if i < n_green else 20}",
                        "=" + places_cursor.clone_shift(y=p_num, x=-1).as_cell_full(),
                    ]
                    + [
                        "=" + sum_plus_cursor.clone_shift(y=p_num, x=k).as_cell_full()
                        for k in range(6)
                    ],
                    add_newline=True,
                    horizontal=True,
                )
        self.sort_cur = self.magic.spawn_cursor(y=26, x=11)
        self.sort_cur.add_block(magic_header("PO-1"), add_newline=True, horizontal=True)
        self.after_prev = self.sort_cur.clone()
        self.sort_cur.add_block(
            [
                f"=SORT(A27:J{27 + total_participants - 1}, 2, 1, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"
            ],
            add_newline=True,
        )
        self.sort_cur.y += total_participants - 1

    def make_zh_reg(self):
        self.reg = self.get_worksheet("Регистрация", remove=True)
        init_worksheet(self.reg)
        cur = self.reg.cursor
        cur.freeze_row()
        cur.hblock_n(
            [
                FCell("Фамилия", Cf.BOLD, width=150),
                FCell("Имя", Cf.BOLD, width=90),
                FCell("Оплата (вовремя)", Cf.BOLD + Cf.CENTER, width=150),
                FCell("На месте", Cf.BOLD, width=80),
                FCell("Посев", Cf.BOLD + Cf.RIGHT, width=80),
            ]
        )
        range_start = cur.clone()
        players_num = self.players_num
        cur.shift(x=2).vblock([FCell(None, Cf.CENTER)] * players_num)
        cur.vblock([FCell(Checkbox(), Cf.CENTER)] * players_num)
        cur.vblock(list(range(1, players_num + 1)))
        cur.hblock_n(
            [FCell("Регистрация завершена", Cf.CENTER + Cf.BOLD, merge_shift_x=1), None]
        )
        reg_is_finished = cur.clone()
        cur.hblock([FCell(Checkbox(), Cf.CENTER, merge_shift_x=1), None])
        range_end = range_start.clone_shift(x=3, y=players_num - 1)
        is_present_range_start = range_start.clone_shift(x=3)
        is_present_range_end = is_present_range_start.clone_shift(y=players_num - 1)
        posev_range_start = range_start.clone_shift(x=4)
        posev_range_end = posev_range_start.clone_shift(y=players_num - 1)
        cur.shift(y=-1).hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({range_start}:{range_end}, """
                    f"""{is_present_range_start}:{is_present_range_end}, FALSE, """
                    f"""{posev_range_start}:{posev_range_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        second_range_start = cur.clone_shift(x=-1)
        second_range_end = second_range_start.clone_shift(x=1, y=players_num - 1)
        second_zh_start = second_range_start.clone_shift(x=4)
        second_zh_end = second_zh_start.clone_shift(y=players_num - 1)
        third_zh_start = second_zh_start.clone_shift(x=1)
        third_zh_end = third_zh_start.clone_shift(y=players_num - 1)
        cur.shift(x=3, y=-1)
        cur.hblock_n(
            [
                FCell("Жеребьёвка1", Cf.RIGHT),
                FCell("Жеребьёвка2", Cf.RIGHT),
            ]
        )
        if self.players_num in (36, 48):
            self.matches_in_round = 12
        else:
            self.matches_in_round = 8
        if self.players_num in (32, 48):
            self.players_in_group_match = 4
        elif self.players_num == 36:
            self.players_in_group_match = 3
        else:
            raise NotImplementedError
        groups = []
        for _ in range(self.players_in_group_match):
            group = list(range(1, self.matches_in_round + 1))
            self.rnd.shuffle(group)
            groups.extend(group)
        cur.vblock(groups)
        zher2 = []
        for _ in range(players_num):
            zher2.append(self.rnd.randint(1, 1000))
        cur.vblock(zher2)
        cur.shift(y=-1).hblock_n(
            [
                FCell("Фамилия", Cf.BOLD),
                FCell("Имя", Cf.BOLD),
                FCell("Группа", Cf.BOLD + Cf.RIGHT),
            ]
        )
        cur.hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({second_range_start}:{second_range_end}, """
                    f"""{second_zh_start}:{second_zh_end}, TRUE, """
                    f"""{third_zh_start}:{third_zh_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        self.init_qual_cells = {}
        cur.shift(x=1)
        for n in range(6):
            second_range_start.clone_shift(x=n).hide_col()
        for n in range(1, self.matches_in_round + 1):
            group_start = cur.clone()
            cur.vblock_n([n] * self.players_in_group_match)
            group_end = cur.clone_shift(y=-1)
            self.init_qual_cells[n] = GroupRange(start=group_start, end=group_end)

    def make_si(self, players_num=48, po_scheme=PlayoffEnum.PO24_2):
        assert players_num == 32
        self.players_num = players_num
        self.match_counter = 1
        self.rnd = random.SystemRandom()
        self.make_zh_reg()
        self.plosch = self.get_worksheet("Площадки", remove=True)
        init_worksheet(self.plosch)
        self.plosch.hblock_n(
            [
                FCell("№", Cf.BOLD + Cf.RIGHT, width=40),
                FCell("Место", Cf.BOLD, width=100),
                FCell("Ведущий", Cf.BOLD, width=150),
                FCell("Ассистент", Cf.BOLD, width=150),
            ]
        )
        self.plosch.vblock(list(range(1, 9)))
        self.po_prot = self.get_worksheet("Плей-офф (протоколы)", remove=True)
        self.po = self.get_worksheet("Плей-офф", remove=True)
        init_worksheet(self.po_prot)
        init_worksheet(self.po)
        self.magic = self.get_worksheet("Magic", remove=True)
        init_worksheet(self.magic)
        self.cur = self.magic.cursor
        self.sort_cur = self.magic.spawn_cursor(x=11, y=1)
        self.sort_cur.add_block(
            magic_header("Groups"), add_newline=True, horizontal=True
        )
        self.add_po_1()
        self.add_po_next(
            stage_num=2,
            hr_prev_stage_num="Первый",
            po_composition=PO_2,
            n_green=4,
        )
        self.add_po_next(
            stage_num=3,
            hr_prev_stage_num="Второй",
            po_composition=PO_3,
            n_green=2,
        )
        fourth_stage_matches = self.add_po_next(
            stage_num=4,
            hr_prev_stage_num="Третий",
            po_composition=PO_4,
            n_green=1,
        )
        fifth_stage_matches = self.add_po_next(
            stage_num=5, hr_prev_stage_num="Четвёртый", po_composition=PO_5, n_green=0
        )
        self.add_po_wo_shuffle(
            matches=[
                [
                    get_place(fifth_stage_matches[0], 1),
                    get_place(fifth_stage_matches[0], 2),
                    get_place(fifth_stage_matches[1], 1),
                    get_place(fifth_stage_matches[1], 2),
                ]
            ],
            n_green=0,
            stage_name="Финал нижней сетки",
        )
        # fourth_stage_matches = self.add_po_wo_shuffle(
        #     matches=[
        #         [
        #             get_place(third_stage_matches[0], 1),
        #             get_place(third_stage_matches[0], 2),
        #             get_place(third_stage_matches[1], 1),
        #             get_place(third_stage_matches[1], 2),
        #         ],
        #         [
        #             get_place(third_stage_matches[0], 1),
        #             get_place(third_stage_matches[0], 2),
        #             get_place(third_stage_matches[1], 1),
        #             get_place(third_stage_matches[1], 2),
        #         ],
        #     ],
        #     n_green=0,
        #     stage_name="4 этап",
        # )
        # comp_dict = PLAYOFF_COMPOSITION[self.po_scheme]
        # if self.po_scheme == PlayoffEnum.PO24_2:
        #     self.add_po_next(
        #         stage_num=2,
        #         hr_prev_stage_num="Первый",
        #         po_composition=comp_dict["PO_3"],
        #         n_green=2,
        #     )
        #     self.add_po_next(
        #         stage_num=3,
        #         hr_prev_stage_num="Второй",
        #         po_composition=comp_dict["PO_4"],
        #         n_green=1,
        #     )  # upper bracket final
        #     lower_bracket_semifinals = self.add_po_next(
        #         stage_num=4,
        #         hr_prev_stage_num="Третий",
        #         po_composition=comp_dict["PO_5"],
        #         n_green=0,
        #         skip_matches_for_stats=2,
        #     )
        #     self.add_po_wo_shuffle(
        #         matches=[
        #             [
        #                 get_place(lower_bracket_semifinals[0], 1),
        #                 get_place(lower_bracket_semifinals[0], 2),
        #                 get_place(lower_bracket_semifinals[1], 1),
        #                 get_place(lower_bracket_semifinals[1], 2),
        #             ]
        #         ],
        #         n_green=0,
        #         stage_name="Финал нижней сетки",
        #     )  # lower bracket final
        # elif self.po_scheme == PlayoffEnum.PO16:
        #     second_round = self.add_po_next(
        #         stage_num=2,
        #         hr_prev_stage_num="Первый",
        #         po_composition=comp_dict["PO_2"],
        #         n_green=2,
        #     )
        #     third_round = self.add_po_next(
        #         stage_num=3,
        #         hr_prev_stage_num="Второй",
        #         po_composition=comp_dict["PO_3"],
        #         n_green=0,
        #     )
        #     fourth_round = self.add_po_wo_shuffle(
        #         matches=[
        #             [
        #                 get_place(second_round[0], 1),
        #                 get_place(second_round[0], 2),
        #                 get_place(second_round[1], 1),
        #                 get_place(second_round[1], 2),
        #             ],
        #             [
        #                 get_place(third_round[0], 1),
        #                 get_place(third_round[0], 2),
        #                 get_place(third_round[1], 1),
        #                 get_place(third_round[1], 2),
        #             ],
        #         ],
        #         n_green=1,
        #         stage_name="4 этап",
        #     )
        #     self.add_po_wo_shuffle(
        #         matches=[
        #             [
        #                 get_place(fourth_round[0], 3),
        #                 get_place(fourth_round[0], 4),
        #                 get_place(fourth_round[1], 1),
        #                 get_place(fourth_round[1], 2),
        #             ],
        #         ],
        #         n_green=1,
        #         stage_name="Финал нижней сетки",
        #     )
        # else:
        #     raise NotImplementedError
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    parser.add_argument("--players_num", "-pn", type=int, default=32)
    parser.add_argument(
        "--po_scheme",
        "-ps",
        type=enumtype,
        default=PlayoffEnum.PO24_2.name,
    )
    args = parser.parse_args()
    assert args.players_num == 32

    gc = gspread.service_account(args.service_account)

    writer = OnlineOtborMaker(gc, args.spreadsheet_id)
    writer.make_si(players_num=args.players_num, po_scheme=args.po_scheme)


if __name__ == "__main__":
    main()
