#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    FCell,
)
from studchr_tables_common.kinsbf import KinsbfMaker
from studchr_tables_common.utils import (
    LIGHT_GRAY_BG,
    init_worksheet,
)


class StudchrKinsbf(KinsbfMaker):
    PF_GROUPS = [[1, 8, 9], [2, 7, 10], [3, 6, 11], [4, 5, 12]]

    def make_zh_reg(self):
        self.zh_reg = self.get_worksheet("Жеребьёвка", remove=True)
        init_worksheet(self.zh_reg)
        checkbox_cursor = self.zh_reg.spawn_cursor(x=2, y=2)
        self.zh_finished = checkbox_cursor.clone()
        checkbox_cursor.hblock([FCell(Checkbox(), Cf.CENTER), "Жеребьёвка завершена"])
        cursor = self.zh_reg.cursor
        self.venues_to_cursors = {}
        for basket_num in range(1, 5):
            cursor.set(y=4, x=1 + (basket_num - 1) * 3)
            cursor.hblock([FCell(None, width=48)])
            cursor.hblock_n(
                [
                    FCell(
                        f"Корзина {basket_num}",
                        Cf.CENTER + LIGHT_GRAY_BG,
                        width=54,
                        merge_shift_x=1,
                    ),
                    FCell(None, width=250),
                ]
            )
            cursor.hblock_n([FCell("Группа", Cf.ITALIC), FCell("Команда", Cf.ITALIC)])
            cursor.vblock([FCell(x, Cf.CENTER) for x in "ABCDEFGHIJKL"])
            self.venues_to_cursors[basket_num] = cursor.clone()
        self.plosch = self.get_worksheet("Площадки", remove=True)
        init_worksheet(self.plosch)
        self.plosch.hblock_n(
            [
                FCell("№", Cf.BOLD + Cf.RIGHT, width=40),
                FCell("Место", Cf.BOLD, width=100),
                FCell("Ведущий", Cf.BOLD, width=150),
                FCell("Ассистент", Cf.BOLD, width=150),
            ]
        )
        self.plosch.vblock(list(range(1, 7)))

    def make_kinsbf(self):
        self.match_counter = 1
        self.group_counter = 1
        self.make_zh_reg()
        self.ind_stats = self.get_worksheet("Индивидуальная статистика", remove=True)
        init_worksheet(self.ind_stats)
        self.ind_stats.cursor.add_block(
            [
                FCell("Бой", Cf.ITALIC, width=50),
                FCell("Команда", Cf.ITALIC, width=200),
                FCell("Игрок", Cf.ITALIC, width=150),
                FCell("1/0", Cf.ITALIC, width=20),
            ],
            horizontal=True,
            add_newline=True,
        )

        self.for_reshuffle = self.get_worksheet(
            "Для пересева", remove=True, hidden=True
        )
        init_worksheet(self.for_reshuffle)
        self.for_reshuffle.add_block(
            [
                FCell("Бой", Cf.ITALIC, width=50),
                FCell("Команда", Cf.ITALIC, width=200),
                FCell("Взятые (б/п)", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Вопр.", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Разница", Cf.ITALIC + Cf.RIGHT, width=50),
                FCell("Очки", Cf.ITALIC + Cf.RIGHT, width=50),
            ],
            horizontal=True,
            add_newline=True,
        )

        self.sost = self.get_worksheet("Составы", hidden=True)
        self.setka = self.get_worksheet("Сетка", remove=True)
        init_worksheet(self.setka)
        self.setka.cursor.add_block(
            [
                FCell(None, width=30),
                FCell("1-й групповой этап, 1-й заход", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.setka.cursor.add_block([FCell(None, height=26)])
        self.group_tab_1 = self.get_worksheet("1-й групповой этап", remove=True)
        init_worksheet(self.group_tab_1)
        self.protocols_1 = self.get_worksheet(
            "1-й групповой этап (протоколы)", remove=True
        )
        init_worksheet(self.protocols_1)
        header = [FCell("Первый заход", Cf.font_size(13), height=36)]
        self.protocols_1.cursor.add_block([FCell(None, width=30)])
        self.protocols_1.cursor.add_block(header, add_newline=True)
        self.group_tab_1.cursor.add_block([FCell(None, width=30)])
        self.group_tab_1.cursor.add_block(header, add_newline=True)
        group_cursors_1 = []
        for i in range(6):
            group_cursors_1.append(
                self.add_group(
                    [
                        self.init_participant(basket=1, participant=i),
                        self.init_participant(basket=2, participant=i),
                        self.init_participant(basket=3, participant=i),
                        self.init_participant(basket=4, participant=i),
                    ],
                    self.group_tab_1.spawn_cursor(
                        x=2 + 12 * (i % 2), y=2 + 7 * (i // 2)
                    ),
                    self.protocols_1.cursor,
                    self.setka.cursor,
                    suffix=f" (пл. {i + 1})",
                    stats_for_reshuffle=True,
                )
            )
        self.setka.cursor.set(x=self.setka.max_col() + 1, y=1)
        self.setka.cursor.add_block([FCell(None, width=30)], horizontal=True)
        self.setka.cursor.add_block(
            [
                FCell("1-й групповой этап, 2-й заход", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        ffr = group_cursors_1[-2].first_free_row
        self.group_tab_1.cursor.set(x=ffr.x, y=ffr.y)
        self.group_tab_1.cursor.y += 1
        header = [FCell("Второй заход", Cf.font_size(13), height=36)]
        self.group_tab_1.cursor.add_block(header, horizontal=True, add_newline=True)
        self.protocols_1.cursor.y += 1
        self.protocols_1.cursor.add_block(header, horizontal=True, add_newline=True)
        group_cursors_2 = []
        for i in range(6):
            group_cursors_2.append(
                self.add_group(
                    [
                        self.init_participant(basket=1, participant=i + 6),
                        self.init_participant(basket=2, participant=i + 6),
                        self.init_participant(basket=3, participant=i + 6),
                        self.init_participant(basket=4, participant=i + 6),
                    ],
                    self.group_tab_1.spawn_cursor(
                        x=2 + 12 * (i % 2), y=3 + 7 * (i // 2 + 3)
                    ),
                    self.protocols_1.cursor,
                    self.setka.cursor,
                    suffix=f" (пл. {i + 1})",
                    stats_for_reshuffle=True,
                )
            )
        self.de_tab = self.get_worksheet("Double Elimination", remove=True)
        self.de_tab.add_block([FCell(None, width=30)])
        init_worksheet(self.de_tab)
        self.protocols_de = self.get_worksheet("DE (протоколы)", remove=True)
        self.protocols_de.add_block([FCell(None, width=30)])
        init_worksheet(self.protocols_de)
        self.setka.cursor.set(x=self.setka.max_col() + 1, y=1)
        self.setka.cursor.add_block([FCell(None, width=30)], horizontal=True)
        self.setka.cursor.add_block(
            [
                FCell("Double Elimination", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        group_cursors_de = []
        for i in range(6):
            if i % 2:
                j = i - 1
            else:
                j = i + 1
            group_cursors_de.append(
                self.add_de(
                    [
                        "=" + group_cursors_1[i].get_place_formula(1),
                        "=" + group_cursors_1[j].get_place_formula(2),
                        "=" + group_cursors_2[i].get_place_formula(1),
                        "=" + group_cursors_2[j].get_place_formula(2),
                    ],
                    self.de_tab.cursor,
                    self.protocols_de.cursor,
                    self.setka.cursor,
                    suffix=f" (пл. {i + 1})",
                    stats_for_reshuffle=True,
                )
            )
        self.reshuffle = self.get_worksheet("Пересев", remove=True)
        init_worksheet(self.reshuffle)
        de_finished_checkbox = self.reshuffle.cursor.clone()
        self.reshuffle.add_block(
            [Checkbox(), "DE закончен"], horizontal=True, add_newline=True
        )
        reshuffle_header = [
            FCell("Команда", Cf.ITALIC, width=200),
            FCell("% очков", Cf.ITALIC, width=60),
            FCell("% взятых", Cf.ITALIC, width=60),
            FCell("Разница", Cf.ITALIC, width=60),
            FCell("Toss", Cf.ITALIC, width=40),
        ]
        self.reshuffle.add_block(reshuffle_header, horizontal=True, add_newline=True)
        rs_start = self.for_reshuffle.spawn_cursor(x=1, y=2)
        rs_end = self.for_reshuffle.cursor.clone_shift(x=6, y=-1)

        def rs_shift_range(n):
            cl = rs_start.clone_shift(x=n)
            return cl.as_cell_full() + ":" + cl.clone().set(y=rs_end.y).as_cell_full()

        def add_reshuffle_block(team):
            team_cell = self.reshuffle.cursor.clone().as_cell_full()

            def get_filter(n):
                return f"""FILTER({rs_shift_range(n)}, {rs_shift_range(1)} = {team_cell})"""

            total_points = f"(COUNTA({get_filter(1)}) * 2)"
            team_points = f"""SUM({get_filter(5)})"""
            team_questions = f"""SUM({get_filter(2)})"""
            team_questions_total = f"""SUM({get_filter(3)})"""
            team_razn = f"""SUM({get_filter(4)})"""
            points_share = f"{team_points}/{total_points}"
            questions_share = f"{team_questions}/{team_questions_total}"
            self.reshuffle.add_block(
                [
                    "=" + team,
                    "=IFNA(" + points_share + ", 0)",
                    "=IFNA(" + questions_share + ", 0)",
                    "=IFNA(" + team_razn + ", -99)",
                    0,
                ],
                horizontal=True,
                add_newline=True,
            )

        for gc in group_cursors_de:
            for pl in (1, 2):
                team = gc.get_place_formula(pl)
                add_reshuffle_block(team)
        self.reshuffle.add_block(reshuffle_header, horizontal=True, add_newline=True)
        reshuffle_start = self.reshuffle.cursor.clone()
        reshuffle_sort_range = (
            reshuffle_start.replace(y=3).as_cell_full()
            + ":"
            + reshuffle_start.clone_shift(
                y=-2, x=len(reshuffle_header) - 1
            ).as_cell_full()
        )
        sort_formula = f"""=IF({de_finished_checkbox.as_cell_full()}, SORT({reshuffle_sort_range}, 2, FALSE, 3, FALSE, 4, FALSE, 5, FALSE), "Ждём конца DE")"""
        self.reshuffle.add_block([sort_formula])
        self.reshuffle.shift(y=20)
        self.reshuffle.add_block(["Конец пересева"])

        self.setka.cursor.set(x=self.setka.max_col() + 1, y=1)
        self.setka.cursor.add_block([FCell(None, width=30)], horizontal=True)
        self.setka.cursor.add_block(
            [
                FCell("Предфинальные группы", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.group_tab_2 = self.get_worksheet("Предфинальные группы", remove=True)
        init_worksheet(self.group_tab_2)
        self.group_tab_2.add_block([FCell(None, width=30)])
        self.protocols_2 = self.get_worksheet("ПФ-группы (протоколы)", remove=True)
        init_worksheet(self.protocols_2)
        self.protocols_2.add_block([FCell(None, width=30)])
        group_cursors_3 = []
        for i in range(4):
            group_cursors_3.append(
                self.add_group(
                    [
                        "=" + reshuffle_start.clone_shift(y=x - 1).as_cell_full()
                        for x in self.PF_GROUPS[i]
                    ],
                    self.group_tab_2.spawn_cursor(
                        x=2 + 12 * (i % 2), y=2 + 7 * (i // 2)
                    ),
                    self.protocols_2.cursor,
                    self.setka.cursor,
                    suffix=f" (пл. {i + 1})",
                    stats_for_reshuffle=True,
                )
            )

        self.setka.cursor.set(x=self.setka.max_col() + 1, y=1)
        self.setka.cursor.add_block([FCell(None, width=30)], horizontal=True)
        self.setka.cursor.add_block(
            [
                FCell("Финальные группы", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.final_tab = self.get_worksheet("Финальный этап", remove=True)
        init_worksheet(self.final_tab)
        self.final_tab.add_block([FCell(None, width=30)])
        self.final_group_prot = self.get_worksheet("Финальный этап, часть 1 (протоколы)", remove=True)
        init_worksheet(self.final_group_prot)
        self.final_group_prot.add_block([FCell(None, width=30)])
        group_cursors_4 = []
        for i in range(2):
            group_cursors_4.append(
                self.add_group(
                    [
                        "=" + group_cursors_3[0].get_place_formula(2 if i else 1),
                        "=" + group_cursors_3[1].get_place_formula(1 if i else 2),
                        "=" + group_cursors_3[2].get_place_formula(2 if i else 1),
                        "=" + group_cursors_3[3].get_place_formula(1 if i else 2),
                    ],
                    self.final_tab.spawn_cursor(x=2 + 12 * (i % 2), y=2 + 7 * (i // 2)),
                    self.final_group_prot.cursor,
                    self.setka.cursor,
                    suffix=f" (пл. {i + 1})",
                )
            )
        self.final_prot = self.get_worksheet(
            "Финальный этап, часть 2 (протоколы)", remove=True
        )
        init_worksheet(self.final_prot)
        self.final_tab.cursor.set(x=26, y=1)
        self.final_tab.hblock_n([FCell("Полуфиналы", Cf.font_size(13))])
        self.setka.cursor.set(x=self.setka.max_col() + 1, y=1)
        self.setka.cursor.add_block([FCell(None, width=30)], horizontal=True)
        self.setka.cursor.add_block(
            [
                FCell("Полуфиналы", Cf.ITALIC, width=150),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.final_prot.cursor.hblock_n([FCell("Первый полуфинал", Cf.font_size(13))])
        polu1 = self.add_match(
            [
                "=" + group_cursors_4[0].get_place_formula(1),
                "=" + group_cursors_4[1].get_place_formula(2),
            ],
            self.final_prot.cursor,
            add_tiebreak=True,
            n_questions=7,
        )
        for cursor in (self.setka.cursor, self.final_tab.cursor):
            self.add_table_de_match(polu1, cursor)
            cursor.shift(y=1)
        self.final_prot.cursor.clone_shift(y=-1).hblock_n(
            [FCell("Второй полуфинал", Cf.font_size(13))]
        )
        polu2 = self.add_match(
            [
                "=" + group_cursors_4[1].get_place_formula(1),
                "=" + group_cursors_4[0].get_place_formula(2),
            ],
            self.final_prot.cursor,
            add_tiebreak=True,
            n_questions=7,
        )
        for cursor in (self.setka.cursor, self.final_tab.cursor):
            self.add_table_de_match(polu2, cursor)
            cursor.shift(y=1)

        def next_stage(cursor):
            cursor.shift(x=2)
            cursor.hblock([FCell(None, width=30)])
            cursor.set(y=1)

        def add_match_name_header(match_name):
            self.final_tab.hblock_n([FCell(match_name, Cf.font_size(13))])
            self.setka.hblock_n([FCell(match_name, Cf.ITALIC)])
            self.final_prot.clone_shift(y=-1).hblock_n([FCell(match_name, Cf.font_size(13))])

        for cursor in (self.setka.cursor, self.final_tab.cursor):
            next_stage(cursor)
        add_match_name_header("Матч за третье место")
        m3 = self.add_match(
            [
                "=" + polu1.loser_formula,
                "=" + polu2.loser_formula,
            ],
            self.final_prot.cursor,
            add_tiebreak=True,
            n_questions=7,
        )
        for cursor in (self.setka.cursor, self.final_tab.cursor):
            self.add_table_de_match(m3, cursor)
            next_stage(cursor)
        self.final_prot.cursor.set(x=1, y=self.final_prot.max_row() + 2)
        for n in range(1, 4):
            add_match_name_header(f"Финал. Бой {n}")
            mc = self.add_match(
                [
                    "=" + polu1.winner_formula,
                    "=" + polu2.winner_formula,
                ],
                self.final_prot.cursor,
                add_tiebreak=True,
            )
            for cursor in (self.setka.cursor, self.final_tab.cursor):
                self.add_table_de_match(mc, cursor)
                cursor.shift(y=1)
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = StudchrKinsbf(gc, args.spreadsheet_id)
    writer.make_kinsbf()


if __name__ == "__main__":
    main()
