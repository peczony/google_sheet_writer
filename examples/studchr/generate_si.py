#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import random
from typing import NamedTuple

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    Cursor,
    FCell,
    col_to_a,
)
from studchr_tables_common.si import SiMaker, magic_header, get_place
from studchr_tables_common.utils import (
    GREEN_BG,
    LIGHT_GRAY_BG,
    init_worksheet,
)


"""
To launch in studchr mode:

python generate_si.py -s 1uefriP3xUsdZQVI64dxXKXlygarxeBLN1CuweuPocVg --variant studchr

To launch in mosotbor mode:

python generate_si.py -s 1WrQIMpPfnZ8hZrMdthJB8_PfkkTMc2h-BzSmaynPYH8 --variant mosotbor --po_themes 6,6,6,6,6,8

"""

GROUP_STAGE = [
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
    [[1, 4, 7], [2, 5, 8], [3, 6, 9]],
    [[3, 5, 7], [1, 6, 8], [2, 4, 9]],
    [[1, 5, 9], [2, 6, 7], [3, 4, 8]],
]
PO_1 = [
    [1, 12, 13, 24],
    [2, 11, 14, 23],
    [3, 10, 15, 22],
    [4, 9, 16, 21],
    [5, 8, 17, 20],
    [6, 7, 18, 19],
]
PO_2 = [
    [1, 6, 7, 12],
    [2, 5, 8, 11],
    [3, 4, 9, 10],
    [13, 18, 19, 24],
    [14, 17, 20, 23],
    [15, 16, 21, 22],
]
PO_3 = [[1, 4, 5], [2, 3, 6], [7, 12, 13, 18], [8, 11, 14, 17], [9, 10, 15, 16]]
PO_4 = [[1, 2, 3, 4], [5, 8, 9, 12], [6, 7, 10, 11]]
PO_5 = [[3, 6, 7], [4, 5, 8]]


class GroupCursors(NamedTuple):
    table_cursor: Cursor
    match_cursor: Cursor


def calc_group_points(cur):
    return f'=IF({cur.clone_shift(x=-1).as_cell()}="","",4-{cur.clone_shift(x=-1).as_cell()}+{cur.clone_shift(x=-2).as_cell()}/1000)'


def filter_protocols_by_column(cell, protocols, cursor):
    col = col_to_a(cursor.x)
    sums = []
    for pr in protocols:
        title = protocols[pr].ws.title
        sum_ = (
            f"SUM(IFNA(FILTER('{title}'!{col}1:{col}99, '{title}'!A1:A99 = {cell}), 0))"
        )
        sums.append(sum_)
    return "=" + " + ".join(sums)


def group_header(hidden=False):
    return [
        FCell("Игрок", Cf.ITALIC, width=150, hidden=hidden),
        FCell("Очки", Cf.ITALIC + Cf.RIGHT, width=42, hidden=hidden),
        FCell("Круг 1", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 2", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 3", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("Круг 4", Cf.ITALIC + Cf.RIGHT, width=50, hidden=hidden),
        FCell("П", Cf.ITALIC + Cf.RIGHT, width=30, hidden=True),
    ]


class GroupRange(NamedTuple):
    start: Cursor
    end: Cursor


class StudchrSiMaker(SiMaker):
    def add_group_stage(self):
        self.groups = {}
        self.rounds = {}
        self.gr_to_ws = {}
        self.gr_to_cursors = {}
        for gr in range(1, 7):
            if gr % 2 == 1:
                wks = self.get_worksheet(
                    f"Группы {col_to_a(gr)}-{col_to_a(gr + 1)}", remove=True
                )
                self.groups[gr] = wks
                init_worksheet(wks)
                wks.spawn_cursor(x=8, y=1).add_block(
                    [FCell(f"Группа {col_to_a(gr)}", Cf.BOLD + Cf.font_size(13))]
                )
                self.gr_to_cursors[gr] = GroupCursors(
                    wks.spawn_cursor(x=1, y=2),
                    wks.spawn_cursor(x=16, y=2),
                )
            else:
                self.groups[gr] = self.get_worksheet(
                    f"Группы {col_to_a(gr - 1)}-{col_to_a(gr)}"
                )
                wks.spawn_cursor(x=8, y=28).add_block(
                    [FCell(f"Группа {col_to_a(gr)}", Cf.BOLD + Cf.font_size(13))]
                )
                self.gr_to_cursors[gr] = GroupCursors(
                    wks.spawn_cursor(x=1, y=29),
                    wks.spawn_cursor(x=16, y=29),
                )
            orig_cur = self.gr_to_cursors[gr].table_cursor
            # orig_cur.clone_shift(x=12).set_col_width(25)
            orig_cur.clone().add_block(
                group_header(hidden=True),
                horizontal=True,
            )
            cur = orig_cur.clone_shift(x=7)
            cur.add_block(
                group_header(),
                horizontal=True,
                add_newline=True,
            )
            cur.wks.parent.fmt[cur.wks.ws.title]["cell_ranges"].append(
                (
                    f"{cur.as_cell()}:{cur.clone_shift(x=6, y=3).as_cell()}",
                    GREEN_BG,
                )
            )
            cur.add_block(
                [
                    lambda _: f"=SORT({orig_cur.clone_shift(y=1).as_cell()}:{orig_cur.clone_shift(x=6, y=9).as_cell()}, 2, FALSE, 7, FALSE)"
                ]
            )
            cur = orig_cur.clone_shift(y=1)
            cur.add_block(
                [
                    "=" + self.venues_to_ranges[gr][i - 1].as_cell_full()
                    for i in range(1, 10)
                ]
            )
            cur.add_block(
                [
                    lambda cur: f"=SUM({cur.clone_shift(x=1).as_cell()}:{cur.clone_shift(x=4).as_cell()})"
                    for _ in range(1, 10)
                ]
            )
        for round in range(1, 5):
            wks = self.get_worksheet(f"Круг {round} (протоколы)", remove=True)
            self.rounds[round] = wks
            init_worksheet(wks)
            id_round = round - 1
            round_composition = GROUP_STAGE[id_round]
            sum_plus_cursor = None
            for gr in self.groups:
                group_cursors = self.gr_to_cursors[gr]
                table_cursor = group_cursors.table_cursor
                match_cursor = group_cursors.match_cursor
                match_cursor.clone_shift(y=6 * id_round, x=-1).add_block(
                    [FCell(f"Круг {round}", Cf.BOLD + Cf.RIGHT, width=120)]
                )
                wks.add_block(
                    [FCell(f"Группа {col_to_a(gr)}", Cf.font_size(13), height=40)],
                    add_newline=True,
                )
                for n, match in enumerate(round_composition):
                    participants = [
                        "=" + table_cursor.clone_shift(y=i).as_cell_full()
                        for i in match
                    ]
                    match_cursor.clone_shift(y=6 * id_round, x=5 * (n % 3)).add_block(
                        [FCell(None, LIGHT_GRAY_BG)] * 4, horizontal=True
                    )
                    match_cursors = self.add_si_match(
                        match_cursor.clone_shift(y=6 * id_round, x=5 * (n % 3)),
                        self.rounds[round].cursor,
                        participants,
                        themes=6,
                        match_id=f"{col_to_a(gr)}{id_round * 3 + n + 1}",
                        italic_headline=True,
                    )
                    places_cursor = match_cursors.places_cursor
                    if sum_plus_cursor is None:
                        sum_plus_cursor = match_cursors.sum_plus_cursor
                    places_cursor.clone_shift(x=1).add_block(
                        [FCell("Очки", Cf.ITALIC, width=42, align="RIGHT")]
                        + [calc_group_points for _ in participants]
                    )
                    places_cursor.clone_shift(x=2).set_col_width(
                        46
                    )  # space between group matches in table
                    p = 1
                    for i in match:
                        table_cursor.clone_shift(y=i, x=round + 1).add_block(
                            ["=" + places_cursor.clone_shift(x=1, y=p).as_cell()]
                        )
                        p += 1
        magic = self.get_worksheet("Magic", remove=True)
        init_worksheet(magic)
        cur = magic.cursor
        cur.add_block(
            magic_header("Groups", hidden=True),
            add_newline=True,
            horizontal=True,
        )
        for gr in self.groups:
            group_cursors = self.gr_to_cursors[gr]
            table_cursor = group_cursors.table_cursor.clone_shift(x=7)
            for i in range(4):
                table_cursor.shift(y=1)
                cur.add_block(
                    [
                        "=" + table_cursor.as_cell_full(),
                        i + 1,  # place
                        "=" + table_cursor.clone_shift(x=1).as_cell_full(),
                    ]
                    + [
                        filter_protocols_by_column(
                            cur.as_cell(), self.rounds, sum_plus_cursor.clone_shift(x=i)
                        )
                        for i in range(6)
                    ],
                    add_newline=True,
                    horizontal=True,
                )
        self.cur = cur
        self.magic = magic
        self.sort_cur = magic.spawn_cursor(x=11, y=1)
        self.sort_cur.add_block(
            magic_header("Groups"), add_newline=True, horizontal=True
        )
        self.after_gr = self.sort_cur.clone()
        if self.variant == "studchr":
            sort_func = "=SORT(A2:J25, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"
        elif self.variant == "mosotbor":
            sort_func = "=SORT(A2:J25, 2, 1, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"
        else:
            raise ValueError
        self.sort_cur.add_block(
            [sort_func]
        )

    def add_po_1(self):
        po = self.po
        po_prot = self.po_prot
        cur = self.cur  # self.cur is magic cursor, self.sort_cur is magic cursor set to sorted range of previous stage
        bc = po_prot.cursor.add_block(
            [
                FCell(
                    "Групповой этап окончен",
                    Cf.WRAP + Cf.RIGHT,
                    height=40,
                    merge_shift_x=1,
                ),
                None,
                FCell(Checkbox(), Cf.CENTER),
            ],
            horizontal=True,
            add_newline=True,
        )
        po_prot.cursor.y += 1
        group_stage_finished = po_prot.spawn_cursor(x=bc.max_x, y=bc.max_y)
        po.cursor.add_block([FCell(None, width=34)])
        po.cursor.set(x=2, y=1)
        po.add_block(
            [FCell("1 этап", Cf.font_size(13))], horizontal=True, add_newline=True
        )
        po.cursor.shift(y=1)
        po_prot.cursor.add_block(
            [FCell("1 этап", Cf.font_size(13), height=40)], add_newline=True
        )
        cur.y = 26
        cur.x = 1
        cur.add_block(
            magic_header("PO-1", hidden=True),
            add_newline=True,
            horizontal=True,
        )
        for n, match in enumerate(PO_1):
            po.cursor.clone().add_block([FCell(None, GREEN_BG)] * 3, horizontal=True)
            po.cursor.clone_shift(y=1, x=-1).add_block(match)
            match_cursors = self.add_si_match(
                po.cursor,
                po_prot.cursor,
                [
                    f'=IF({group_stage_finished.as_cell_full()}, {self.after_gr.clone_shift(y=n - 1).as_cell_full()}, "Группы-{n}")'
                    for n in match
                ],
                add_newline=True,
                match_id_suffix=f" (пл. {n + 1})",
                add_tiebreak=True,
                themes=self.po_themes[0],
            )
            places_cursor = match_cursors.places_cursor
            sum_plus_cursor = match_cursors.sum_plus_cursor
            for i in range(len(match)):
                p_num = i + 1
                cur.add_block(
                    [
                        "=" + places_cursor.clone_shift(y=p_num, x=-2).as_cell_full(),
                        "=" + places_cursor.clone_shift(y=p_num).as_cell_full(),
                        "=" + places_cursor.clone_shift(y=p_num, x=-1).as_cell_full(),
                    ]
                    + [
                        "=" + sum_plus_cursor.clone_shift(y=p_num, x=i).as_cell_full()
                        for i in range(6)
                    ],
                    add_newline=True,
                    horizontal=True,
                )
        self.sort_cur = self.magic.spawn_cursor(y=26, x=11)
        self.sort_cur.add_block(magic_header("PO-1"), add_newline=True, horizontal=True)
        self.after_prev = self.sort_cur.clone()
        self.sort_cur.add_block(
            ["=SORT(A27:J50, 2, 1, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0)"],
            add_newline=True,
        )
        self.sort_cur.y += 23

    def make_zh_reg(self):
        self.reg = self.get_worksheet("Регистрация", remove=True)
        init_worksheet(self.reg)
        self.plosch = self.get_worksheet("Площадки", remove=True)
        init_worksheet(self.plosch)
        self.plosch.hblock_n(
            [
                FCell("№", Cf.BOLD + Cf.RIGHT, width=40),
                FCell("Место", Cf.BOLD, width=100),
                FCell("Ведущий", Cf.BOLD, width=150),
                FCell("Ассистент", Cf.BOLD, width=150),
            ]
        )
        self.plosch.vblock(list(range(1, 7)))
        cur = self.reg.cursor
        cur.freeze_row()
        cur.hblock_n(
            [
                FCell("Игрок", Cf.BOLD, width=180),
                FCell("DEPRECATED", Cf.BOLD, width=90, hidden=True),
                FCell("Оплата (вовремя)", Cf.BOLD + Cf.CENTER, width=150),
                FCell("На месте", Cf.BOLD, width=80),
                FCell("Посев", Cf.BOLD + Cf.RIGHT, width=80),
            ]
        )
        range_start = cur.clone()
        cur.shift(x=2).vblock([FCell(None, Cf.CENTER)] * 54)
        cur.vblock([FCell(Checkbox(), Cf.CENTER)] * 54)
        cur.vblock(list(range(1, 55)))
        cur.hblock_n(
            [FCell("Регистрация завершена", Cf.CENTER + Cf.BOLD, merge_shift_x=1), None]
        )
        reg_is_finished = cur.clone()
        cur.hblock([FCell(Checkbox(), Cf.CENTER, merge_shift_x=1), None])
        range_end = range_start.clone_shift(x=3, y=53)
        is_present_range_start = range_start.clone_shift(x=3)
        is_present_range_end = is_present_range_start.clone_shift(y=53)
        posev_range_start = range_start.clone_shift(x=4)
        posev_range_end = posev_range_start.clone_shift(y=53)
        cur.shift(y=-1).hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({range_start}:{range_end}, """
                    f"""{is_present_range_start}:{is_present_range_end}, FALSE, """
                    f"""{posev_range_start}:{posev_range_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        second_range_start = cur.clone_shift(x=-1)
        second_range_end = second_range_start.clone_shift(x=1, y=53)
        second_zh_start = second_range_start.clone_shift(x=4)
        second_zh_end = second_zh_start.clone_shift(y=53)
        third_zh_start = second_zh_start.clone_shift(x=1)
        third_zh_end = third_zh_start.clone_shift(y=53)
        cur.shift(x=3, y=-1)
        cur.hblock_n(
            [
                FCell("Жеребьёвка1", Cf.RIGHT),
                FCell("Жеребьёвка2", Cf.RIGHT),
            ]
        )
        groups = []
        for _ in range(9):
            group = list(range(1, 7))
            self.rnd.shuffle(group)
            groups.extend(group)
        cur.vblock(groups)
        zher2 = []
        for _ in range(54):
            zher2.append(self.rnd.randint(1, 1000))
        cur.vblock(zher2)
        cur.shift(y=-1).hblock_n(
            [
                FCell("Игрок", Cf.BOLD, width=180),
                FCell("DEPRECATED", Cf.BOLD, hidden=True),
                FCell("Группа", Cf.BOLD + Cf.RIGHT),
            ]
        )
        cur.hblock(
            [
                FCell(
                    f"""=IF({reg_is_finished}, SORT({second_range_start}:{second_range_end}, """
                    f"""{second_zh_start}:{second_zh_end}, TRUE, """
                    f"""{third_zh_start}:{third_zh_end}, TRUE), "Регистрация ещё идёт")"""
                )
            ]
        )
        self.group_cells = {}
        cur.shift(x=1)
        for n in range(6):
            second_range_start.clone_shift(x=n).hide_col()
        for n in range(1, 7):
            group_start = cur.clone()
            cur.vblock_n([n] * 9)
            group_end = cur.clone_shift(y=-1)
            self.group_cells[n] = GroupRange(start=group_start, end=group_end)
        self.groups_wks = self.get_worksheet("Группы", remove=True)
        init_worksheet(self.groups_wks)
        cur = self.groups_wks.cursor
        cur.hblock_n([FCell(None, width=15)] + [FCell(None, width=135)] * 8)
        self.venues_to_ranges = {}
        for i in range(1, 7):
            if i >= 4:
                y_shift = 13
            else:
                y_shift = 0
            if i % 3 == 1:
                x_shift = 0
            elif i % 3 == 2:
                x_shift = 3
            elif i % 3 == 0:
                x_shift = 6
            cur.set(x=2 + x_shift, y=2 + y_shift)
            cur.hblock_n(
                [
                    FCell(
                        f"Группа {col_to_a(i)}", Cf.BOLD + Cf.CENTER, merge_shift_x=1
                    ),
                    None,
                ]
            )
            cur.hblock_n(
                [
                    FCell(
                        self.get_plosch_formula(i),
                        Cf.ITALIC + Cf.CENTER,
                        merge_shift_x=1,
                    ),
                    None,
                ]
            )
            self.venues_to_ranges[i] = [cur.clone_shift(y=j) for j in range(9)]
            for j in range(9):
                cur.hblock_n(
                    [
                        FCell(
                            "="
                            + self.group_cells[i]
                            .start.clone_shift(y=j, x=-2)
                            .as_cell_full()
                        ),
                        FCell(
                            "="
                            + self.group_cells[i]
                            .start.clone_shift(y=j, x=-1)
                            .as_cell_full()
                        ),
                    ]
                )
    
    def make_gf(self, upper_bracket_final, lower_bracket_final):
        finals_participants = [
            get_place(upper_bracket_final, 1),
            get_place(upper_bracket_final, 2),
            get_place(lower_bracket_final, 1),
            get_place(lower_bracket_final, 2),
        ]
        self.gf_prot = self.get_worksheet("Грандфинал (протокол)", remove=True)
        init_worksheet(self.gf_prot)
        finals = self.add_po_wo_shuffle(
            matches=[finals_participants],
            n_green=1,
            stage_name="Грандфинал",
            themes=12,
            override_po_prot=self.gf_prot,
        )[0]
        for place in (4, 3, 2, 1):
            self.final_places.cursor.hblock_n([get_place(finals, place), str(place)])
        tableau = self.get_worksheet("Табло", remove=True)
        init_worksheet(tableau)
        T_WIDTH = 300
        T_HEIGHT = 90
        T_FONT_SIZE = 20
        T_STYLE = Cf.MIDDLE + Cf.CENTER + Cf.font_size(T_FONT_SIZE)
        tableau.cursor.hblock_n(
            [
                FCell(x, T_STYLE, width=T_WIDTH, height=T_HEIGHT)
                for x in finals_participants
            ]
        )
        tableau.cursor.hblock_n(
            [
                FCell(
                    "=" + finals.sums_cursor.clone_shift(y=x + 1).as_cell_full(),
                    T_STYLE,
                    height=T_HEIGHT,
                )
                for x in range(len(finals_participants))
            ]
        )

    def make_si(self, po_themes="8,8,8,8,8,8", variant="studchr"):
        self.variant = variant
        self.po_themes = [int(x.strip()) for x in po_themes.split(",") if x.strip()]
        self.rnd = random.SystemRandom()
        if variant == "studchr":
            self.stats = self.get_worksheet(
                "Индивидуальная статистика", remove=True, hidden=True
            )
            init_worksheet(self.stats)
            self.stats.cursor.hblock_n(
                [
                    FCell("Бой", Cf.BOLD, width=60),
                    FCell("Игрок", Cf.BOLD, width=180),
                    FCell("Σ", Cf.BOLD + Cf.RIGHT, width=40),
                ]
            )
        self.make_zh_reg()
        self.add_group_stage()
        self.po_prot = self.get_worksheet("Плей-офф (протоколы)", remove=True)
        self.po = self.get_worksheet("Плей-офф", remove=True)
        init_worksheet(self.po_prot)
        init_worksheet(self.po)
        self.match_counter = 7
        self.add_po_1()
        stage2 = self.add_po_next(
            stage_num=2,
            hr_prev_stage_num="Первый",
            po_composition=PO_2,
            n_green=3,
            themes=self.po_themes[1],
        )
        if variant == "studchr":
            self.final_places = self.get_worksheet("Места", remove=True, hidden=True)
            init_worksheet(self.final_places)
            self.final_places.cursor.hblock_n(
                [FCell("Игрок", width=180), FCell("Место", align="CENTER", width=40)]
            )
            for mc in stage2[3:]:
                for place in (3, 4):
                    self.final_places.cursor.hblock_n([get_place(mc, place), "19–24"])
        stage3 = self.add_po_next(
            stage_num=3,
            hr_prev_stage_num="Второй",
            po_composition=PO_3,
            n_green=2,
            themes=self.po_themes[2],
        )
        if variant == "studchr":
            for mc in stage3[2:]:
                for place in (3, 4):
                    self.final_places.cursor.hblock_n([get_place(mc, place), "13–18"])
        stage4 = self.add_po_next(
            stage_num=4,
            hr_prev_stage_num="Третий",
            po_composition=PO_4,
            n_green=1,
            themes=self.po_themes[3],
        )
        if variant == "studchr":
            upper_bracket_final = stage4[0]
            for mc in stage4[1:]:
                for place in (3, 4):
                    self.final_places.cursor.hblock_n([get_place(mc, place), "9–12"])
        lower_bracket_semifinals = self.add_po_next(
            stage_num=5,
            hr_prev_stage_num="Четвёртый",
            po_composition=PO_5,
            n_green=0,
            skip_matches_for_stats=2,
            themes=self.po_themes[4],
            add_magic=False,
        )
        if variant == "studchr":
            for mc in lower_bracket_semifinals:
                self.final_places.cursor.hblock_n([get_place(mc, 3), "7–8"])
        lower_bracket_final = self.add_po_wo_shuffle(
            matches=[
                [
                    get_place(lower_bracket_semifinals[0], 1),
                    get_place(lower_bracket_semifinals[0], 2),
                    get_place(lower_bracket_semifinals[1], 1),
                    get_place(lower_bracket_semifinals[1], 2),
                ]
            ],
            n_green=0,
            stage_name="Финал нижней сетки",
            themes=self.po_themes[5],
        )[0]
        if variant == "studchr":
            for place in (3, 4):
                self.final_places.cursor.hblock_n(
                    [get_place(lower_bracket_final, place), str(place + 2)]
                )
            self.make_gf(
                upper_bracket_final,
                lower_bracket_final,
            )
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    parser.add_argument("--po_themes", default="8,8,8,8,8,8")
    parser.add_argument("--variant", choices=["studchr", "mosotbor"], required=True)
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = StudchrSiMaker(gc, args.spreadsheet_id)
    writer.make_si(po_themes=args.po_themes, variant=args.variant)


if __name__ == "__main__":
    main()
