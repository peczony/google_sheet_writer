#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import gspread
from google_sheet_writer import (
    Cf,
    Checkbox,
    FCell,
    col_to_a,
)
from studchr_tables_common.ek import EkMaker, get_place
from studchr_tables_common.utils import (
    LIGHT_GRAY_BG,
    init_worksheet,
)


class StudchrEk(EkMaker):
    COMPOSITION_1_4 = [[1, 8, 9], [4, 5, 12], [2, 7, 10], [3, 6, 11]]

    def make_zh_reg(self):
        self.zh_reg = self.get_worksheet("Жеребьёвка", remove=True)
        init_worksheet(self.zh_reg)
        checkbox_cursor = self.zh_reg.spawn_cursor(x=2, y=2)
        self.zh_finished = checkbox_cursor.clone()
        checkbox_cursor.hblock([FCell(Checkbox(), Cf.CENTER), "Жеребьёвка завершена"])
        cursor = self.zh_reg.cursor
        self.venues_to_cursors = {}
        for basket_num in range(1, 5):
            cursor.set(y=4, x=1 + (basket_num - 1) * 3)
            cursor.hblock([FCell(None, width=48)])
            cursor.hblock_n(
                [
                    FCell(
                        f"Корзина {basket_num}",
                        Cf.CENTER + LIGHT_GRAY_BG,
                        width=54,
                        merge_shift_x=1,
                    ),
                    FCell(None, width=250),
                ]
            )
            cursor.hblock_n([FCell("Бой", Cf.ITALIC + Cf.CENTER), FCell("Команда", Cf.ITALIC)])
            cursor.vblock([FCell(x, Cf.CENTER) for x in "ABCDEFGHIJKL"])
            self.venues_to_cursors[basket_num] = cursor.clone()
        self.plosch = self.get_worksheet("Площадки", remove=True)
        init_worksheet(self.plosch)
        self.plosch.hblock_n(
            [
                FCell("№", Cf.BOLD + Cf.RIGHT, width=40),
                FCell("Место", Cf.BOLD, width=100),
                FCell("Ведущий", Cf.BOLD, width=150),
                FCell("Ассистент", Cf.BOLD, width=150),
            ]
        )
        self.plosch.vblock(list(range(1, 7)))

    def make_ek(self):
        self.match_counter = 1
        self.make_zh_reg()
        self.team_stats = self.get_worksheet(
            "Командная статистика", remove=True, defer=True
        )
        self.ind_stats = self.get_worksheet(
            "Индивидуальная статистика", remove=True, defer=True
        )
        init_worksheet(self.team_stats)
        init_worksheet(self.ind_stats)
        self.team_stats.add_block(
            [
                FCell("Бой", Cf.ITALIC, width=70),
                FCell("Команда", Cf.ITALIC, width=188),
                FCell("Сумма", Cf.ITALIC, width=55),
                FCell("Место", Cf.ITALIC, width=55),
                FCell("Σ+", Cf.ITALIC, align="RIGHT", width=40),
                FCell("50", Cf.ITALIC, width=40),
                FCell("40", Cf.ITALIC, width=40),
                FCell("30", Cf.ITALIC, width=40),
                FCell("20", Cf.ITALIC, width=40),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.ind_stats.add_block(
            [
                FCell("Бой", Cf.ITALIC, width=70),
                FCell("Тема", Cf.ITALIC, width=70),
                FCell("Команда", Cf.ITALIC, width=188),
                FCell("Игрок", Cf.ITALIC, width=175),
                FCell("Очки", Cf.ITALIC, align="RIGHT", width=55),
            ],
            horizontal=True,
            add_newline=True,
        )
        self.tables = self.get_worksheet("Сетка", remove=True)
        init_worksheet(self.tables)
        self.prot_16 = self.get_worksheet("1/16", remove=True)
        init_worksheet(self.prot_16)
        self.sost = self.get_worksheet("Составы")
        self.tables.cursor.add_block(
            [
                FCell("1/16, первый заход", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        match_cursors_16_1 = []
        for i in range(6):
            match_cursors_16_1.append(
                self.add_ek_match(
                    self.tables.cursor,
                    self.prot_16.cursor,
                    [
                        self.init_participant(basket=1, participant=i),
                        self.init_participant(basket=2, participant=i),
                        self.init_participant(basket=3, participant=i),
                        self.init_participant(basket=4, participant=i),
                    ],
                    add_newline=True,
                    match_id=col_to_a(self.match_counter),
                )
            )
        self.tables.cursor.set(x=4, y=1)
        self.tables.cursor.add_block([FCell(None, width=35)], horizontal=True)
        self.tables.cursor.add_block(
            [
                FCell("1/16, второй заход", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        match_cursors_16_2 = []
        for i in range(6, 12):
            match_cursors_16_2.append(
                self.add_ek_match(
                    self.tables.cursor,
                    self.prot_16.cursor,
                    [
                        self.init_participant(basket=1, participant=i),
                        self.init_participant(basket=2, participant=i),
                        self.init_participant(basket=3, participant=i),
                        self.init_participant(basket=4, participant=i),
                    ],
                    add_newline=True,
                    match_id=col_to_a(self.match_counter),
                )
            )
        self.tables.cursor.set(x=8, y=1)
        self.tables.cursor.add_block([FCell(None, width=35)], horizontal=True)
        self.tables.cursor.add_block(
            [
                FCell("1/8 финала", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        self.prot_8 = self.get_worksheet("1/8", remove=True)
        init_worksheet(self.prot_8)
        self.peresev = self.get_worksheet("Пересев перед 1/4", remove=True)
        init_worksheet(self.peresev)
        self.peresev.add_block(
            [
                FCell("Команда", Cf.ITALIC, width=188, hidden=True),
                FCell("Сумма мест", Cf.ITALIC, align="RIGHT", width=90, hidden=True),
                FCell("Сумма", Cf.ITALIC, width=55, hidden=True),
                FCell("Σ+", Cf.ITALIC, align="RIGHT", width=40, hidden=True),
                FCell("50", Cf.ITALIC, width=40, hidden=True),
                FCell("40", Cf.ITALIC, width=40, hidden=True),
                FCell("30", Cf.ITALIC, width=40, hidden=True),
                FCell("20", Cf.ITALIC, width=40, hidden=True),
                FCell("Жребий", Cf.ITALIC, width=50, hidden=True),
            ],
            horizontal=True,
            add_newline=True,
        )

        def add_peresev_block(team):
            block = [team]
            for col in ("D", "C", "E", "F", "G", "H", "I"):
                block.append(
                    lambda x: f"=SUMIF('Командная статистика'!B2:B49, {x.replace(x=1).as_cell()}, 'Командная статистика'!{col}2:{col}49) + SUMIF('Командная статистика'!B50:B73, {x.replace(x=1).as_cell()}, 'Командная статистика'!{col}50:{col}73)"
                )
            self.peresev.add_block(block, horizontal=True, add_newline=True)

        match_cursors_8 = []
        for i in range(6):
            if i % 2:
                j = i - 1
            else:
                j = i + 1
            match_cursors_8.append(
                self.add_ek_match(
                    self.tables.cursor,
                    self.prot_8.cursor,
                    [
                        get_place(match_cursors_16_1[i], 1),
                        get_place(match_cursors_16_2[i], 1),
                        get_place(match_cursors_16_1[j], 2),
                        get_place(match_cursors_16_2[j], 2),
                    ],
                    add_newline=True,
                    match_id=col_to_a(self.match_counter),
                )
            )
        for mc in match_cursors_8:
            for pl in (1, 2):
                add_peresev_block(get_place(mc, pl))
        sorted_cur = self.peresev.spawn_cursor(x=11, y=1)
        sorted_cur.add_block(
            [
                FCell("Место", Cf.ITALIC, align="RIGHT", width=55, hidden=False),
                FCell("Команда", Cf.ITALIC, width=188, hidden=False),
                FCell("Сумма мест", Cf.ITALIC, align="RIGHT", width=90, hidden=False),
                FCell("Сумма", Cf.ITALIC, width=55, hidden=False),
                FCell("Σ+", Cf.ITALIC, align="RIGHT", width=40, hidden=True),
                FCell("50", Cf.ITALIC, width=40, hidden=False),
                FCell("40", Cf.ITALIC, width=40, hidden=False),
                FCell("30", Cf.ITALIC, width=40, hidden=False),
                FCell("20", Cf.ITALIC, width=40, hidden=False),
                FCell("Жребий", Cf.ITALIC, width=50, hidden=False),
            ],
            horizontal=True,
            add_newline=True,
        )
        sorted_cur.add_block(list(range(1, 13)))
        peresev_cur = sorted_cur.clone()
        peresev_cur.clone().add_block(
            [
                "=SORT(A2:I13, 2, TRUE, 3, FALSE, 4, FALSE, 5, FALSE, 6, FALSE, 7, FALSE, 8, FALSE, 9, FALSE)"
            ]
        )
        tick = peresev_cur.clone_shift(x=9)
        tick.add_block(["Бои 1/8 завершены", Checkbox()], add_newline=True)
        tick.shift(y=-1)
        self.tables.cursor.set(x=12, y=1)
        self.tables.cursor.add_block([FCell(None, width=35)], horizontal=True)
        self.tables.cursor.add_block(
            [
                FCell("Четвертьфиналы", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        self.prot_4 = self.get_worksheet("1/4", remove=True)
        init_worksheet(self.prot_4)
        match_cursors_4 = []
        for match in self.COMPOSITION_1_4:
            participants = []
            for participant_id in match:
                participants.append(
                    f"""=IF({tick.as_cell_full()}, {peresev_cur.clone_shift(y=participant_id - 1).as_cell_full()}, "Пересев-{participant_id}")"""
                )
            match_cursors_4.append(
                self.add_ek_match(
                    self.tables.cursor,
                    self.prot_4.cursor,
                    participants,
                    add_newline=True,
                    match_id=col_to_a(self.match_counter),
                )
            )
        self.tables.cursor.set(x=16, y=1)
        self.tables.cursor.add_block([FCell(None, width=35)], horizontal=True)
        self.tables.cursor.add_block(
            [
                FCell("Полуфиналы", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        self.prot_2 = self.get_worksheet("1/2", remove=True)
        init_worksheet(self.prot_2)
        match_cursors_2 = []
        match_cursors_2.append(
            self.add_ek_match(
                self.tables.cursor,
                self.prot_2.cursor,
                [
                    get_place(match_cursors_4[0], 1),
                    get_place(match_cursors_4[1], 2),
                    get_place(match_cursors_4[2], 1),
                    get_place(match_cursors_4[3], 2),
                ],
                add_newline=True,
                match_id=col_to_a(self.match_counter),
            )
        )
        match_cursors_2.append(
            self.add_ek_match(
                self.tables.cursor,
                self.prot_2.cursor,
                [
                    get_place(match_cursors_4[0], 2),
                    get_place(match_cursors_4[1], 1),
                    get_place(match_cursors_4[2], 2),
                    get_place(match_cursors_4[3], 1),
                ],
                add_newline=True,
                match_id=col_to_a(self.match_counter),
            )
        )
        self.tables.cursor.set(x=20, y=1)
        self.tables.cursor.add_block([FCell(None, width=35)], horizontal=True)
        self.tables.cursor.add_block(
            [
                FCell("Финал", Cf.ITALIC, width=188),
            ],
            add_newline=True,
            horizontal=True,
        )
        self.prot_final = self.get_worksheet("Финал", remove=True)
        init_worksheet(self.prot_final)
        finals_participants = [
            get_place(match_cursors_2[0], 1),
            get_place(match_cursors_2[0], 2),
            get_place(match_cursors_2[1], 1),
            get_place(match_cursors_2[1], 2),
        ]
        finals = self.add_ek_match(
            self.tables.cursor,
            self.prot_final.cursor,
            finals_participants,
            add_newline=True,
            match_id=col_to_a(self.match_counter),
        )
        tableau = self.get_worksheet("Табло", remove=True)
        init_worksheet(tableau)
        T_WIDTH = 300
        T_HEIGHT = 90
        T_FONT_SIZE = 20
        T_STYLE = Cf.MIDDLE + Cf.CENTER + Cf.font_size(T_FONT_SIZE)
        tableau.cursor.hblock_n(
            [
                FCell(x, T_STYLE, width=T_WIDTH, height=T_HEIGHT)
                for x in finals_participants
            ]
        )
        tableau.cursor.hblock_n(
            [
                FCell(
                    "=" + finals.sums_cursor.clone_shift(y=x + 1).as_cell_full(),
                    T_STYLE,
                    height=T_HEIGHT,
                )
                for x in range(len(finals_participants))
            ]
        )
        self.submit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--spreadsheet_id", "-s")
    parser.add_argument("--service_account", "-sa", default="service_account.json")
    args = parser.parse_args()

    gc = gspread.service_account(args.service_account)

    writer = StudchrEk(gc, args.spreadsheet_id)
    writer.make_ek()


if __name__ == "__main__":
    main()
